import {
  Component,
  OnInit,
  Input,
  Output,
  SimpleChanges,
  EventEmitter,
  ViewChild,
} from "@angular/core";
import {
  MatTreeFlatDataSource,
  MatTreeFlattener,
  MatTreeNestedDataSource,
} from "@angular/material/tree";
import { FlatTreeControl, NestedTreeControl } from "@angular/cdk/tree";
import { MatMenuTrigger } from "@angular/material/menu";

interface Node {
  Id: string;
  Name: string;
  ParentId: string;
  Level: string;
  Children?: Node[];
}

interface FlatNode {
  Id: string;
  Expandable: boolean;
  Name: string;
  ParentId: string;
  Level: string;
  SplitGroup: any;
  Children?: FlatNode[];
}

@Component({
  selector: "app-tree",
  template: `
    <mat-tree [dataSource]="dataSource" [treeControl]="treeControl">
      <mat-nested-tree-node
        *matTreeNodeDef="let node"
        (click)="nodeLabel(node)"
      >
        <li class="{{ treeControl.isExpanded(node) ? 'root' : 'subroot' }}">
          <div
            class="mat-tree-node"
            [ngClass]="{ color_red: childrenNode == node.Id }"
            (click)="toggleActive(node)"
            (contextmenu)="onRightClick($event, node)"
            style="cursor: pointer;"
          >
            {{ node.Name }}
          </div>
        </li>
      </mat-nested-tree-node>
      <mat-nested-tree-node *matTreeNodeDef="let node; when: hasChildren">
        <li class="example-tree-container">
          <div class="mat-tree-node" (contextmenu)="onRightClick($event, node)">
            <button
              mat-icon-button
              matTreeNodeToggle
              [attr.aria-label]="'toggle ' + node.filename"
              (click)="isExpandNode(node)"
            >
              <mat-icon>
                {{
                  treeControl.isExpanded(node)
                    ? "arrow_drop_down"
                    : "arrow_right"
                }}
              </mat-icon>
            </button>
            {{ node.Name }}
          </div>
          <ul class="example-tree-nested-node">
            <div *ngIf="treeControl.isExpanded(node)">
              <ng-container matTreeNodeOutlet></ng-container>
            </div>
          </ul>
        </li>
      </mat-nested-tree-node>
    </mat-tree>
    <div
      style="visibility: hidden; position: fixed;"
      [style.left]="menuTopLeftPosition.x"
      [style.top]="menuTopLeftPosition.y"
      [matMenuTriggerFor]="rightMenu"
    ></div>
    <mat-menu #rightMenu="matMenu">
      <ng-template matMenuContent let-item="item">
        <button mat-menu-item (click)="expandThis()" *ngIf="isExpand">
          Select tree
        </button>

        <button mat-menu-item (click)="collpaseThis()" *ngIf="isCollapse">
          Restore
        </button>
      </ng-template>
    </mat-menu>
  `,
  styleUrls: ["../../app.component.css"],
})
export class TreeComponent {
  @Input() dataSrc: any;
  @Output() nodeLabelChange = new EventEmitter();
  @Output() dataLabelChange = new EventEmitter();

  @ViewChild(MatMenuTrigger) matMenuTrigger: MatMenuTrigger;

  nodeArray = [];
  @Input() childrenNode: any;
  @Input() treeArray: any;
  selected: any;
  treeControl: NestedTreeControl<FlatNode>;
  dataSource: MatTreeNestedDataSource<FlatNode>;
  SelectedOdm: FlatNode;
  menuTopLeftPosition = { x: "0", y: "0" };
  isExpand: boolean = true;
  isCollapse: boolean = false;
  constructor() {
    this.treeControl = new NestedTreeControl<FlatNode>((node) => node.Children);
    this.dataSource = new MatTreeNestedDataSource();
    console.log(this.treeArray);
  }
  ngOnChanges(changes: SimpleChanges) {
    if (changes.dataSrc) {
      this.dataSource.data = this.dataSrc;
      this.treeControl.dataNodes = this.dataSrc;
      if (this.treeArray && this.treeArray.length != 0) {
        this.treeArray.filter((element) => {
          for (let i = 0; i < this.treeControl.dataNodes.length; i++) {
            if (changes.dataSrc.currentValue[i].Name == element) {
              this.treeControl.expand(this.treeControl.dataNodes[i]);
            }
          }
        });
      }
    }
  }

  hasChildren = (_: number, node: FlatNode) => {
    return node.Children.length > 0;
  };
  isExpandNode(node) {
    console.log("asdas");
    // node.isExpandNode = !node.isExpandNode;
    this.dataNode(node);
  }

  nodeLabel(node) {
    this.childrenNode = node.Id;
    this.nodeLabelChange.emit(node);
  }
  dataNode(node) {
    this.nodeArray = [];
    if (localStorage.getItem("ubtTree")) {
      let prvArrData = JSON.parse(localStorage.getItem("ubtTree"));
      //alert(this.treeControl.isExpanded(node));
      console.log(prvArrData);
      prvArrData.forEach((i, j) => {
        this.nodeArray.push(i);
      });
    }
    console.log(this.treeControl.isExpanded(node));
    if (!this.treeControl.isExpanded(node)) {
      this.nodeArray.splice(this.nodeArray.indexOf(node.Name), 1);
    } else {
      this.SelectedOdm = {
        Name: node.Name,
        Expandable: node.Children.length > 0,
        Id: node.Id,
        Level: node.Level,
        ParentId: node.ParentId,
        SplitGroup: node.SplitGroup,
        Children: node.Children,
      };

      this.treeControl.dataNodes.forEach((element) => {
        if (
          element.Children &&
          this.SelectedOdm.Id == element.Id &&
          this.SelectedOdm.Name == element.Name
        ) {
          if (this.nodeArray.indexOf(node.Name) < 0)
            this.nodeArray.push(node.Name);
        }
      });
      console.log(this.nodeArray);
    }
    console.log(this.nodeArray);
    localStorage.setItem("ubtTree", JSON.stringify(this.nodeArray));
  }

  toggleActive(event) {
    this.selected = event;
    // this.dataNode(event);
  }
  isActive(item) {
    return this.selected === item;
  }
  onRightClick(event: MouseEvent, node) {
    // preventDefault avoids to show the visualization of the right-click menu of the browser
    event.preventDefault();

    // we record the mouse position in our object
    this.menuTopLeftPosition.x = event.clientX + "px";
    this.menuTopLeftPosition.y = event.clientY + "px";

    // we open the menu
    // we pass to the menu the information about our object
    this.matMenuTrigger.menuData = node;
    console.log(node);
    console.log(this.isExpand);
    console.log(this.isCollapse);

    if (
      this.isExpand &&
      node.Children.length == 0 &&
      this.dataSource.data.length == 1
    ) {
      this.isExpand = true;
    }

    if (
      !this.isExpand &&
      node.Children.length > 0 &&
      this.dataSource.data.length == 1
    ) {
      this.isExpand = false;
    }
    if (node.Children.length == 0 && this.dataSource.data.length == 1) {
      this.isExpand = true;
    }
    console.log(this.isExpand);
    console.log(this.isCollapse);
    // we open the menu
    this.matMenuTrigger.openMenu();
  }
  expandThis() {
    console.log(this.matMenuTrigger.menuData);
    console.log(this.selected);
    this.isExpand = false;
    this.isCollapse = true;

    this.dataSource.data = [...this.matMenuTrigger.menuData];
  }
  collpaseThis() {
    this.isExpand = true;
    this.isCollapse = false;
    this.dataSource.data = this.dataSrc;
  }
}
