export class Partner {
    PartnerId:any=null
    PartnerName:any
    PartnerFunction?:any
    PartnerTypeId:any
    Email1:any
    Email2:any
    URL:any
    Phone1:any
    Phone2:any
    Address1:any
    Address2:any
    Address3:any
    StateId:any 
    CIN:any
    GSTIN:any
    PAN:any
    TAN_NO:any
    Status:any=1  
    Fax:any
    IsSource: any = 1
}