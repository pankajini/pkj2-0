export class Employee {
    EmployeeId:String;
    FirstName: string;
    MiddleName: string;
    LastName: string;
    DOB: any;
    FatherName: string;
    MotherName: string;
    Gender: string;
    Religion: string;
    Nationality: string;
    BloodGroup: string;
    IdentityMark: string;
    Remarks: string;
    MaritalStatus: boolean;
    CreatedBy: string = "SUPERADMIN";
    }
    export class EmployeeOfficalInfo {
    EmploymentType: any;
    EmploymentTypeId:any;
    DateOfJoining: any = "01/01/2020"
    DateOfLeaving: any = "01/01/2030";
    ReasonOfLeaving: string;
    LineManager: any;
    EmployeeId:any;
    }
    export class EmployeeAddressInfo {
    HouseNo: any;
    AddressLine1: any;
    AddressLine2: any;
    AddressLine3: any;
    LandMark: any;
    PostOffice: any;
    PoliceStation: any;
    City: any;
    District: any;
    Pin: any;
    StateId: any;
    
    }
    export class EmployeeContactInfo {
    Phone1: any
    Phone2: any
    PersonalEmail: any;
    }
    
    export class EmployeeEmergencyInfo {
    EmergencyContactName:any;
    EmergencyContactPhone1:any;
    EmergencyContactPhone2:any;
    EmergencyContactEmail1:any;
    EmergencyContactEmail2:any;
    EmergencyContactRelationship:any;
    EmergencyDependency:any;
    EmergencyContactOrganization:any;
    }
    export class EmployeeDocumentsInfo{
    AdharNo:any;
    VoterId:any;
    PanNo:any;
    Passport:any;
    RecentPhotographDate:any;
    HighestQualification:any;
    YearOfCompletion:any;
    }
    export class SearchEmploye{
        EmployeeId:any = '';
        FirstName:any = '';
        LastName:any = '';
        ReturnType:any ='';
    }