import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { OprationComponent } from './opration.component';

describe('OprationComponent', () => {
  let component: OprationComponent;
  let fixture: ComponentFixture<OprationComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ OprationComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OprationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
