import { Component, OnInit, TemplateRef, ViewChild } from "@angular/core";
import { MatDialog } from "@angular/material/dialog";
import { ModuleService } from "src/app/modules/module.service";
import { AppService } from "src/app/shared/service/app.service";

@Component({
  selector: "app-user-basic-details",
  templateUrl: "./user-basic-details.component.html",
  styleUrls: ["./user-basic-details.component.css"],
})
export class UserBasicDetailsComponent implements OnInit {
  userName: string;
  userDetails: any;
  allRoles: any = [];
  roleAssociates: any = [];
  standardRoles: any = [];
  newAssignments: any = [];
  oldAssignments: any = [];
  @ViewChild("compareDialog", { static: true }) compareDialog: TemplateRef<any>;

  constructor(
    private appService: AppService,
    private service: ModuleService,
    private dialog: MatDialog
  ) {
    this.userName = this.appService.getParam("userName");
  }

  ngOnInit(): void {
    this.getUserDetailByUserName();
  }
  getUserDetailByUserName() {
    let data = {
      CompanyId: "B02",
      UserName: this.userName,
    };
    let URL = "Account/GetUserDetailByUserNameAndCompanyId_SF";
    this.service.postData(data, URL).subscribe((data: any) => {
      this.userDetails = data[0];
      this.getAllUsersMappedToAllVersions();
    });
  }
  getAllUsersMappedToAllVersions() {
    let data = {
      CompanyId: "B02",
      UserName: this.userName,
    };
    let URL = "Account/GetAllMappedRolesAndUnMappedRolesForAnUser_SF";
    this.service.postData(data, URL).subscribe((data: any) => {
      this.allRoles = [];
      data.MappedRole.forEach((element) => {
        if (element) {
          this.allRoles.push({ RoleName: element.RoleName, Status: true });
        }
      });
      data.UnMappedRole.forEach((element) => {
        if (element) {
          this.allRoles.push({ RoleName: element.RoleName, Status: false });
        }
      });

      this.allRoles = this.allRoles.sort((a, b) => {
        return a.RoleName > b.RoleName;
      });
      console.log(this.allRoles);
      this.changeEvent();
    });
  }
  changeEvent() {
    this.roleAssociates = [];
    this.allRoles.forEach((element) => {
      if (element["Status"] == true) {
        this.roleAssociates.push(element["RoleName"]);
      }
    });
  }

  compareChanges() {
    let RolesAssociated = this.roleAssociates.join(",");
    let data = {
      CompanyId: "B02",
      UserName: this.userName,
      RoleAssociated: RolesAssociated,
    };
    let URL = "Account/CompareAllUserRoleAssignments";
    this.service.postData(data, URL).subscribe((data: any) => {
      let newAssignments = data[0]["standardRoles-NewAssignments"];
      let oldAssignments = data[0]["standardRoles-OldAssignments"];
      this.standardRoles = [];
      this.allRoles.forEach((element) => {
        oldAssignments.forEach((element2) => {
          if (element.RoleName == element2) {
            element.status = "X";
          }
        });
        newAssignments.forEach((element2) => {
          if (element.RoleName == element2) {
            element.status1 = "X";
          }
        });
        this.standardRoles.push(element);
      });
      this.openDialog();
    });
  }
  openDialog() {
    this.dialog.open(this.compareDialog, {
      disableClose: true,
      width: "600px",
    });
  }
  update() {
    let RolesAssociated = this.roleAssociates.join(",");
    let data = {
      CompanyId: "B02",
      UserName: this.userName,
      RoleAssociated: RolesAssociated,
    };
    let URL = "Account/AssignDeassignCompositeAndStandardRolesToUser";
    this.service.postData(data, URL).subscribe((data: any) => {
      this.dialog.closeAll();
      this.getAllUsersMappedToAllVersions();
      if (data.Success) {
        this.appService.showMessage(data.Message, "X");
      } else {
        this.appService.showMessage(data.Message, "X");
      }
    });
  }
}
