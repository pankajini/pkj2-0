import { Component, OnInit } from "@angular/core";
import { AppService } from "src/app/shared/service/app.service";
import { ModuleService } from "../../../module.service";

@Component({
  selector: "app-initial-screen",
  templateUrl: "./initial-screen.component.html",
  styleUrls: ["./initial-screen.component.css"],
})
export class InitialScreenComponent implements OnInit {
  userName: string;
  users: any = [];
  constructor(private service: ModuleService, private appService: AppService) {}

  ngOnInit(): void {
    this.getUsers();
  }
  next() {
    this.appService.navigate("/company-admin/assign-unssign/basic-details", [
      { userName: this.userName },
    ]);
  }
  getUsers() {
    let obj = { CompanyId: "B02" };
    let url = "Account/GetAllUsersByCompanyId_SF";
    this.service.postData(obj, url).subscribe((data: any) => {
      this.users = data;
    });
  }
}
