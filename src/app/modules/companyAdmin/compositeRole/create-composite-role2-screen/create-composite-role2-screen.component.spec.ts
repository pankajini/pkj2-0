import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CreateCompositeRole2ScreenComponent } from './create-composite-role2-screen.component';

describe('CreateCompositeRole2ScreenComponent', () => {
  let component: CreateCompositeRole2ScreenComponent;
  let fixture: ComponentFixture<CreateCompositeRole2ScreenComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CreateCompositeRole2ScreenComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CreateCompositeRole2ScreenComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
