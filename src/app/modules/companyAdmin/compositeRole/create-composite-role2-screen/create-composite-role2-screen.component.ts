import { Component, OnInit, TemplateRef, ViewChild } from "@angular/core";
import { ModuleService } from "src/app/modules/module.service";
import { AppService } from "src/app/shared/service/app.service";
import { ResizeEvent } from "angular-resizable-element";
import { AppComponent } from "src/app/app.component";
import { Employee } from "src/app/shared/entities/employee";
import { MatDialog } from "@angular/material/dialog";
import { CompositeRole } from "../create-composite-role1-screen/compositerole";

@Component({
  selector: "app-create-composite-role2-screen",
  templateUrl: "./create-composite-role2-screen.component.html",
  styleUrls: ["./create-composite-role2-screen.component.css"],
})
export class CreateCompositeRole2ScreenComponent implements OnInit {
  compositeRole: CompositeRole = new CompositeRole();
  searchObj: any;
  height: any = 1;
  previousHeight: number = 43;
  innerHeight: number;
  width: number = 25;
  maxHeight: number;
  restoreHeight: number;
  selectedTab: any = 0;

  isNodelLabelChange: boolean = false;
  childrenNode: any;
  currentEmployeId: any;
  employee: Employee = new Employee();
  dataSrc: any = [];
  compositeRoleName: string;
  isNewCompositeRole: boolean = true;
  isExistingCompositeRole: boolean = false;
  roleAssociates: any = [];
  compositeData: any = [];
  constructor(
    private service: ModuleService,
    private appService: AppService,
    public appComponent: AppComponent
  ) {
    if (this.appService.getParam("compositeRole")) {
      this.compositeRole = JSON.parse(
        this.appService.getParam("compositeRole")
      );
    }
    this.getCompositeRoles();
  }

  ngOnInit(): void {
    this.maxHeight = window.innerHeight - 56;
    if (localStorage.getItem("ubt")) {
      //this.ubt = JSON.parse(localStorage.getItem('ubt'))
    }
    if (localStorage.getItem("ubtTab")) {
      this.selectedTab = localStorage.getItem("ubtTab");
    }
    if (localStorage.getItem("ubtHeight")) {
      this.height = localStorage.getItem("ubtHeight");
    } else {
      this.height = 1;
      this.height = window.innerHeight / 2.2;
    }
  }
  onResizeEnd(event: ResizeEvent) {
    if (event) {
      //  sdfsdf(event.rectangle.height);
      if (
        event.rectangle.height >= 43 &&
        event.rectangle.height <= window.innerHeight - 56
      ) {
        this.height = event.rectangle.height;
        this.previousHeight = this.height;
      }
    }
  }
  minWidth() {
    this.width = 0;
  }
  maxWidth() {
    this.width = 19;
  }
  min() {
    this.height = 1;
  }
  max() {
    if (this.height != window.innerHeight - 90) {
      this.height = window.innerHeight - 90;
    } else {
      this.height = this.previousHeight;
    }
  }
  previous() {
    this.height = this.previousHeight;
  }
  restore() {
    this.height = window.innerHeight / 2.2;
    this.restoreHeight = window.innerHeight / 2.2;
  }
  getCompositeRoles() {
    let data = {
      CompanyId: "B02",
      ReturnType: "TREE",
    };
    let URL = "AdminTree/GetAllStandardAndCompositeRolesByCompanyIdTree_SF";
    this.service.postData(data, URL).subscribe((data: any) => {
      // this.companyId = data.CompanyId;
      this.dataSrc = data.RecursiveObjects;
      console.log(data);
      //  if(this.dataSrc.length != 0){
      //   this.getEmployeDetailsById(this.dataSrc[0].Id)
      //  }
    });
  }
  nodeLabel(node) {
    // this.isEdit = true;

    this.isNodelLabelChange = true;
    if (node) {
      localStorage.setItem("nodeLabel", node);
      this.childrenNode = node;
      console.log(this.childrenNode);
      this.compositeRoleName = this.childrenNode["Name"];
      this.getAllUnderneathRolesTree();
      this.isNewCompositeRole = false;
      this.isExistingCompositeRole = true;
      // this.getEmployeDetailsById(this.childrenNode.Id);
    }
  }

  changeNode() {}
  getAllUnderneathRolesTree() {
    //sdfsdf("hi called");
    let obj = {
      CompanyId: "B02",
      CompositeRoleName: this.compositeRoleName,
      ReturnType: "TREE",
    };
    let url = "AdminTree/GetAllUnderneathRolesTree";
    this.service.postData(obj, url).subscribe((data: any) => {
      // this.employee = data[0];
      // this.commonUserId = data[0].CommonUserId;
      // this.currentEmployeId = this.employee.EmployeeId;
      console.log(data);
      this.compositeData = data["RecursiveObjects"];
    });
  }
  dataNode(event) {}
  changeEvent() {
    this.roleAssociates = [];
    this.dataSrc.forEach((element) => {
      // console.log(element["Status"]);

      element["Children"].forEach((childElement) => {
        // console.log(childElement["Status"]);
        if (childElement["Status"] == true) {
          this.roleAssociates.push(childElement["Name"]);
        }
      });
      console.log(this.roleAssociates);
    });
  }
  create() {
    let RolesAssociated = this.roleAssociates.join(",");
    console.log(RolesAssociated);
    let data = {
      RoleName: this.compositeRole.name,
      CompanyId: "B02",
      RolesAssociated: RolesAssociated,
      Description: this.compositeRole.description,
    };
    let URL = "Account/CreateCompositeRole";
    this.service.postData(data, URL).subscribe((data: any) => {
      console.log(data);
      if (data.Success) {
        this.appService.showMessage("Saved Successfully", "X");
      } else {
        this.appService.showMessage(data.Message, "X");
      }
    });
  }
  newCompositeRole() {
    this.isNewCompositeRole = true;
    this.isExistingCompositeRole = false;
  }
}
