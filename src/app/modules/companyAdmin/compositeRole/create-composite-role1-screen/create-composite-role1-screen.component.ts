import { Component, OnInit, ViewChild } from '@angular/core';
import { NgForm } from '@angular/forms';
import { AppService } from 'src/app/shared/service/app.service';
import {CompositeRole} from './compositerole'

@Component({
  selector: 'app-create-composite-role1-screen',
  templateUrl: './create-composite-role1-screen.component.html',
  styleUrls: ['./create-composite-role1-screen.component.css']
})
export class CreateCompositeRole1ScreenComponent implements OnInit {
  @ViewChild(NgForm, { static: true }) myForm: NgForm;
  compositeRole:CompositeRole =new CompositeRole()
  constructor(private appService:AppService) { 

  }

  ngOnInit(): void {
  }
  onSubmit(myForm){
    console.log(JSON.stringify(this.compositeRole))
    this.appService.navigate('/company-admin/create-composite-role-2',[{'compositeRole':JSON.stringify(this.compositeRole)}])
  }
}
