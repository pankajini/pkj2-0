import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CreateCompositeRole1ScreenComponent } from './create-composite-role1-screen.component';

describe('CreateCompositeRole1ScreenComponent', () => {
  let component: CreateCompositeRole1ScreenComponent;
  let fixture: ComponentFixture<CreateCompositeRole1ScreenComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CreateCompositeRole1ScreenComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CreateCompositeRole1ScreenComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
