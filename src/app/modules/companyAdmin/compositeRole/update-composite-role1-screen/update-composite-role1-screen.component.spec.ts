import { ComponentFixture, TestBed } from '@angular/core/testing';

import { UpdateCompositeRole1ScreenComponent } from './update-composite-role1-screen.component';

describe('UpdateCompositeRole1ScreenComponent', () => {
  let component: UpdateCompositeRole1ScreenComponent;
  let fixture: ComponentFixture<UpdateCompositeRole1ScreenComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ UpdateCompositeRole1ScreenComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(UpdateCompositeRole1ScreenComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
