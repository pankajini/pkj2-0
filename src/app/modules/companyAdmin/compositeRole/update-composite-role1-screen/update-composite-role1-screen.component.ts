import { Component, OnInit } from "@angular/core";
import { AppService } from "src/app/shared/service/app.service";
import { ModuleService } from "../../../module.service";

@Component({
  selector: "app-update-composite-role1-screen",
  templateUrl: "./update-composite-role1-screen.component.html",
  styleUrls: ["./update-composite-role1-screen.component.css"],
})
export class UpdateCompositeRole1ScreenComponent implements OnInit {
  roleName: string;
  compositeRoles: any = [];
  constructor(private service: ModuleService, private appService: AppService) {}

  ngOnInit(): void {
    this.getCompositeRoles();
  }
  search() {
    this.appService.navigate("/company-admin/update-composite-role-2", [
      { compositeRole: this.roleName },
    ]);
  }
  getCompositeRoles() {
    let obj = { CompanyId: "B02" };
    let url = "Account/GetAllCompositeRoleNameByCompanyId_SF";
    this.service.postData(obj, url).subscribe((data: any) => {
      this.compositeRoles = data;
    });
  }
}
