import { ComponentFixture, TestBed } from '@angular/core/testing';

import { UpdateCompositeRole2ScreenComponent } from './update-composite-role2-screen.component';

describe('UpdateCompositeRole2ScreenComponent', () => {
  let component: UpdateCompositeRole2ScreenComponent;
  let fixture: ComponentFixture<UpdateCompositeRole2ScreenComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ UpdateCompositeRole2ScreenComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(UpdateCompositeRole2ScreenComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
