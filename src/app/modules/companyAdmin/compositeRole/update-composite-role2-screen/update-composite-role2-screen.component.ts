import { Component, OnInit, TemplateRef, ViewChild } from "@angular/core";
import { ModuleService } from "src/app/modules/module.service";
import { AppService } from "src/app/shared/service/app.service";
import { ResizeEvent } from "angular-resizable-element";
import { AppComponent } from "src/app/app.component";
import { Employee } from "src/app/shared/entities/employee";
import { MatDialog } from "@angular/material/dialog";
import { CompositeRole } from "../create-composite-role1-screen/compositerole";

@Component({
  selector: "app-update-composite-role2-screen",
  templateUrl: "./update-composite-role2-screen.component.html",
  styleUrls: ["./update-composite-role2-screen.component.css"],
})
export class UpdateCompositeRole2ScreenComponent implements OnInit {
  height: any = 1;
  previousHeight: number = 43;
  innerHeight: number;
  width: number = 25;
  maxHeight: number;
  restoreHeight: number;
  selectedTab: any = 0;

  isNodelLabelChange: boolean = false;
  childrenNode: any;
  currentEmployeId: any;
  employee: Employee = new Employee();
  dataSrc: any = [];
  compositeRole: string;
  roleAssociates: any = [];
  compositeRoleName: string;
  compositeRoleDescription: string;
  allRoles: any = [];
  constructor(
    private service: ModuleService,
    private appService: AppService,
    public appComponent: AppComponent
  ) {
    if (this.appService.getParam("compositeRole")) {
      this.compositeRole = this.appService.getParam("compositeRole");
    }
    this.getCompositeRoles();
  }

  ngOnInit(): void {
    this.maxHeight = window.innerHeight - 56;
    if (localStorage.getItem("ubt")) {
      //this.ubt = JSON.parse(localStorage.getItem('ubt'))
    }
    if (localStorage.getItem("ubtTab")) {
      this.selectedTab = localStorage.getItem("ubtTab");
    }
    if (localStorage.getItem("ubtHeight")) {
      this.height = localStorage.getItem("ubtHeight");
    } else {
      this.height = 1;
      this.height = window.innerHeight / 2.2;
    }
  }
  onResizeEnd(event: ResizeEvent) {
    if (event) {
      //  sdfsdf(event.rectangle.height);
      if (
        event.rectangle.height >= 43 &&
        event.rectangle.height <= window.innerHeight - 56
      ) {
        this.height = event.rectangle.height;
        this.previousHeight = this.height;
      }
    }
  }
  minWidth() {
    this.width = 0;
  }
  maxWidth() {
    this.width = 19;
  }
  min() {
    this.height = 1;
  }
  max() {
    if (this.height != window.innerHeight - 90) {
      this.height = window.innerHeight - 90;
    } else {
      this.height = this.previousHeight;
    }
  }
  previous() {
    this.height = this.previousHeight;
  }
  restore() {
    this.height = window.innerHeight / 2.2;
    this.restoreHeight = window.innerHeight / 2.2;
  }
  getCompositeRoles() {
    let data = {
      CompanyId: "B02",
      CompositeRoleName: this.compositeRole,
      ReturnType: "TREE",
    };
    let URL = "AdminTree/GetAllVersionsForAnCompositeRoleAndCompanyId_SF";
    this.service.postData(data, URL).subscribe((data: any) => {
      // this.companyId = data.CompanyId;
      this.dataSrc = data.RecursiveObjects;
      this.compositeRoleName = this.dataSrc[0]["Name"];
      this.compositeRoleDescription = this.dataSrc[0]["Description"];
      this.getAllMappedRolesAndUnMappedRoles();
      console.log(data);
      //  if(this.dataSrc.length != 0){
      //   this.getEmployeDetailsById(this.dataSrc[0].Id)
      //  }
    });
  }
  nodeLabel(node) {
    // this.isEdit = true;

    this.isNodelLabelChange = true;
    if (node) {
      localStorage.setItem("nodeLabel", node);
      this.childrenNode = node;
      console.log(this.childrenNode);
      this.compositeRoleName = this.childrenNode["Name"];
      this.getAllMappedRolesAndUnMappedRoles();
      // this.isNewCompositeRole = false;
      // this.isExistingCompositeRole = true;
      // this.getEmployeDetailsById(this.childrenNode.Id);
    }
  }

  changeNode() {}
  getAllMappedRolesAndUnMappedRoles() {
    //sdfsdf("hi called");
    let obj = {
      CompanyId: "B02",
      RoleName: this.compositeRoleName,
    };
    let url = "Account/GetAllMappedRolesAndUnMappedRolesForACompositeRole_SF";
    this.service.postData(obj, url).subscribe((data: any) => {
      this.allRoles = [];
      data.MappedRole.forEach((element) => {
        if (element) {
          this.allRoles.push({ RoleName: element.RoleName, Status: true });
        }
      });
      data.UnMappedRole.forEach((element) => {
        if (element) {
          this.allRoles.push({ RoleName: element.RoleName, Status: false });
        }
      });
      // this.employee = data[0];
      // this.commonUserId = data[0].CommonUserId;
      // this.currentEmployeId = this.employee.EmployeeId;
      console.log(data);
    });
  }
  dataNode(event) {}
  changeEvent() {
    this.roleAssociates = [];
    this.allRoles.forEach((element) => {
      console.log(element["Status"]);
      if (element["Status"] == true) {
        this.roleAssociates.push(element["RoleName"]);
      }
      console.log(this.roleAssociates);
    });
  }
  update() {
    let RolesAssociated = this.roleAssociates.join(",");
    console.log(RolesAssociated);
    let data = {
      RoleName: this.compositeRoleName,
      CompanyId: "B02",
      RolesAssociated: RolesAssociated,
      Description: this.compositeRoleDescription,
    };
    let URL = "Account/UpdateCompositeRole";
    this.service.postData(data, URL).subscribe((data: any) => {
      console.log(data);
      if (data.Success) {
        this.appService.showMessage("Saved Successfully", "X");
        this.appService.navigate("/company-admin/migrate-user", [
          { roleName: this.compositeRole },
        ]);
      } else {
        this.appService.showMessage(data.Message, "X");
      }
    });
  }
}
