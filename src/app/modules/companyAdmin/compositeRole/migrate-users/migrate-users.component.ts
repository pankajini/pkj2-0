import { Component, OnInit } from "@angular/core";
import { ModuleService } from "src/app/modules/module.service";
import { AppService } from "src/app/shared/service/app.service";

@Component({
  selector: "app-migrate-users",
  templateUrl: "./migrate-users.component.html",
  styleUrls: ["./migrate-users.component.css"],
})
export class MigrateUsersComponent implements OnInit {
  roleName: string;
  constructor(private appService: AppService, private service: ModuleService) {
    this.roleName = this.appService.getParam("roleName");
  }

  ngOnInit(): void {
    this.getAllUsersMappedToAllVersions();
  }
  getAllUsersMappedToAllVersions() {
    let data = {
      CompanyId: "B02",
      RoleName: this.roleName,
      ReturnType: "FLAT",
    };
    let URL =
      "Account/GetAllUsersMappedToAllVersionsOfACompositeRoleByCompanyId_SF";
    this.service.postData(data, URL).subscribe((data: any) => {
      console.log(data);
    });
  }
}
