import { ComponentFixture, TestBed } from '@angular/core/testing';

import { MigrateUsersComponent } from './migrate-users.component';

describe('MigrateUsersComponent', () => {
  let component: MigrateUsersComponent;
  let fixture: ComponentFixture<MigrateUsersComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ MigrateUsersComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(MigrateUsersComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
