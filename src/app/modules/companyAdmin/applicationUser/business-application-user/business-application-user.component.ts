import { Component, OnInit, TemplateRef, ViewChild } from "@angular/core";
import { ModuleService } from "src/app/modules/module.service";
import { AppService } from "src/app/shared/service/app.service";
import { ResizeEvent } from "angular-resizable-element";
import { AppComponent } from "src/app/app.component";
import { Employee } from "src/app/shared/entities/employee";
import { MatDialog } from "@angular/material/dialog";

@Component({
  selector: "app-business-application-user",
  templateUrl: "./business-application-user.component.html",
  styleUrls: ["./business-application-user.component.css"],
})
export class BusinessApplicationUserComponent implements OnInit {
  searchObj: any;
  height: any = 1;
  previousHeight: number = 43;
  innerHeight: number;
  width: number = 25;
  maxHeight: number;
  restoreHeight: number;
  selectedTab: any = 0;

  isNodelLabelChange: boolean = false;
  childrenNode: any;
  currentEmployeId: any;
  employee: Employee = new Employee();
  dataSrc: any = [];
  commonUserId: any;
  Password: any;
  ConfirmPassword: any;
  companyId: any = "B02";
  userName: any;
  Email: any;
  PhoneNumber: any;
  @ViewChild("createDialog", { static: true }) createDialog: TemplateRef<any>;
  constructor(
    private service: ModuleService,
    private appService: AppService,
    public appComponent: AppComponent,
    private dialog: MatDialog
  ) {
    this.userName = localStorage.getItem("userName");
    if (this.appService.getParam("searchObj")) {
      this.searchObj = JSON.parse(this.appService.getParam("searchObj"));
      this.getEmployees(this.searchObj);
    }
  }

  ngOnInit(): void {
    this.maxHeight = window.innerHeight - 56;
    if (localStorage.getItem("ubt")) {
      //this.ubt = JSON.parse(localStorage.getItem('ubt'))
    }
    if (localStorage.getItem("ubtTab")) {
      this.selectedTab = localStorage.getItem("ubtTab");
    }
    if (localStorage.getItem("ubtHeight")) {
      this.height = localStorage.getItem("ubtHeight");
    } else {
      this.height = 1;
      this.height = window.innerHeight / 2.2;
    }
  }
  onResizeEnd(event: ResizeEvent) {
    if (event) {
      //  sdfsdf(event.rectangle.height);
      if (
        event.rectangle.height >= 43 &&
        event.rectangle.height <= window.innerHeight - 56
      ) {
        this.height = event.rectangle.height;
        this.previousHeight = this.height;
      }
    }
  }
  minWidth() {
    this.width = 0;
  }
  maxWidth() {
    this.width = 19;
  }
  min() {
    this.height = 1;
  }
  max() {
    if (this.height != window.innerHeight - 90) {
      this.height = window.innerHeight - 90;
    } else {
      this.height = this.previousHeight;
    }
  }
  previous() {
    this.height = this.previousHeight;
  }
  restore() {
    this.height = window.innerHeight / 2.2;
    this.restoreHeight = window.innerHeight / 2.2;
  }
  getEmployees(data) {
    let URL = "EmployeeMasterApi/SearchEmployees_SF";
    this.service.postData(data, URL).subscribe((data: any) => {
      this.companyId = data.CompanyId;
      this.dataSrc = data.RecursiveObjects;
      if (this.dataSrc.length != 0) {
        this.getEmployeDetailsById(this.dataSrc[0].Id);
      }
    });
  }
  nodeLabel(node) {
    // this.isEdit = true;

    this.isNodelLabelChange = true;
    if (node) {
      localStorage.setItem("nodeLabel", node);
      this.childrenNode = node;
      this.getEmployeDetailsById(this.childrenNode.Id);
    }
  }

  changeNode() {}
  getEmployeDetailsById(id) {
    //sdfsdf("hi called");
    let obj = {
      EmployeeId: id,
    };
    let url = "EmployeeMasterApi/GetEmployeeDetailsByEmployeeId_SF";
    this.service.postData(obj, url).subscribe((data: any) => {
      this.employee = data[0];
      this.commonUserId = data[0].CommonUserId;
      this.currentEmployeId = this.employee.EmployeeId;
      console.log(this.employee);
    });
  }
  dataNode(event) {}
  create() {
    this.openDialog();
  }
  openDialog() {
    this.dialog.open(this.createDialog, { disableClose: true });
  }
  closeDialog() {
    this.dialog.closeAll();
  }

  createApplicationUser() {
    if (this.Password == this.ConfirmPassword) {
      let obj = {
        UserName: "B02" + "-" + this.commonUserId,
        LoggedInUserId: "B02" + "_" + this.userName,
        Email: this.employee["PersonalEmail"],
        Password: this.Password,
        PhoneNumber: this.employee["Phone1"],
        CompanyId: "B02",
        Type: "Business",
      };
      let url = "Account/CreateApplicationUserForCompany";
      this.service.postData(obj, url).subscribe((data: any) => {
        if (data.Success) {
          this.appService.showMessage(data.Message, "X");
          this.dialog.closeAll();
          this.getEmployees(this.searchObj);
        } else {
          this.appService.showMessage(data.Errors[0], "X");
        }
      });
    } else {
      this.appService.showMessage("Password not matched", "X");
    }
  }
}
