import { ComponentFixture, TestBed } from '@angular/core/testing';

import { BusinessApplicationUserComponent } from './business-application-user.component';

describe('BusinessApplicationUserComponent', () => {
  let component: BusinessApplicationUserComponent;
  let fixture: ComponentFixture<BusinessApplicationUserComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ BusinessApplicationUserComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(BusinessApplicationUserComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
