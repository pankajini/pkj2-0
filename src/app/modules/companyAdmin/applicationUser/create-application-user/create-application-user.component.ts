import { Component, OnInit, ViewChild } from '@angular/core';
import { NgForm } from '@angular/forms';
import { ModuleService } from 'src/app/modules/module.service';
import { SearchEmploye } from 'src/app/shared/entities/employee';
import { AppService } from 'src/app/shared/service/app.service';

@Component({
  selector: 'app-create-application-user',
  templateUrl: './create-application-user.component.html',
  styleUrls: ['./create-application-user.component.css']
})
export class CreateApplicationUserComponent implements OnInit {
  searchEmployee:SearchEmploye = new SearchEmploye();
  @ViewChild(NgForm, { static: true }) myForm: NgForm;
  dataSrc:any;
  constructor(private service :ModuleService,private appService:AppService) { }
  
  ngOnInit(): void {
  }
  onSubmit(event){
    console.log(event)

    this.searchEmployee.ReturnType = 'TREE';
    // let URL = "EmployeeMasterApi/SearchEmployees_SF";
    // this.service.postData(this.searchEmployee, URL).subscribe((data: any) => {
    //   console.log(data)
    // })
    var string = JSON.stringify(this.searchEmployee)
    console.log(string)
    this.appService.navigate('/company-admin/create-business-user',[{searchObj:string}])
  }
}
