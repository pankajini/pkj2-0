import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AssociateInitialScreenComponent } from './associate-initial-screen.component';

describe('AssociateInitialScreenComponent', () => {
  let component: AssociateInitialScreenComponent;
  let fixture: ComponentFixture<AssociateInitialScreenComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AssociateInitialScreenComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AssociateInitialScreenComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
