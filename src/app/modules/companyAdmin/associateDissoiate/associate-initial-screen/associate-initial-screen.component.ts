import { Component, OnInit } from "@angular/core";
import { ModuleService } from "src/app/modules/module.service";
import { AppService } from "src/app/shared/service/app.service";

@Component({
  selector: "app-associate-initial-screen",
  templateUrl: "./associate-initial-screen.component.html",
  styleUrls: ["./associate-initial-screen.component.css"],
})
export class AssociateInitialScreenComponent implements OnInit {
  userName: string;
  users: any = [];
  constructor(private service: ModuleService, private appService: AppService) {}

  ngOnInit(): void {
    this.getUsers();
  }
  next() {
    this.appService.navigate(
      "/company-admin/associate-disassociate/basic-details",
      [{ userName: this.userName }]
    );
  }
  getUsers() {
    let obj = { CompanyId: "B02" };
    let url = "Account/GetAllUsersByCompanyId_SF";
    this.service.postData(obj, url).subscribe((data: any) => {
      this.users = data;
    });
  }
}
