import { Component, OnInit, TemplateRef, ViewChild } from "@angular/core";
import { MatDialog } from "@angular/material/dialog";
import { ModuleService } from "src/app/modules/module.service";
import { AppService } from "src/app/shared/service/app.service";

@Component({
  selector: "app-associate-user-basic-details",
  templateUrl: "./associate-user-basic-details.component.html",
  styleUrls: ["./associate-user-basic-details.component.css"],
})
export class AssociateUserBasicDetailsComponent implements OnInit {
  userName: string;
  userDetails: any;
  username: any;
  password: any;
  @ViewChild("loginDialog", { static: true }) loginDialog: TemplateRef<any>;
  constructor(
    private dialog: MatDialog,
    private appService: AppService,
    private service: ModuleService
  ) {
    this.userName = this.appService.getParam("userName");
  }

  ngOnInit(): void {
    this.getUserDetailByUserName();
  }
  getUserDetailByUserName() {
    let data = {
      CompanyId: "B02",
      UserName: this.userName,
    };
    let URL = "Account/GetUserDetailByUserNameAndCompanyId_SF";
    this.service.postData(data, URL).subscribe((data: any) => {
      console.log(data);
      this.userDetails = data[0];
      console.log(this.userDetails);
    });
  }
  setValue(e) {
    console.log(e.checked);
    if (e.checked) {
      this.userDetails.IsActive = "true";
    } else {
      this.userDetails.IsActive = "false";
    }
    this.openDialog();
  }
  openDialog() {
    // this.isCheckForm = true;
    // this.dialog.open(this.statusDialog, { disableClose: true });

    this.dialog.open(this.loginDialog, { disableClose: true });
  }
  login() {
    let obj = {
      username: this.username,
      password: this.password,
      Type: "Admin",
      grant_type: "password",
    };

    this.service.conformlogin(obj).subscribe((data: any) => {
      console.log(data);
      if (data["User Name"]) {
        this.associateDissociateUser();
      }
    });
  }
  associateDissociateUser() {
    let IsActive: any;
    console.log(this.userDetails.IsActive);
    if (this.userDetails.IsActive == true) {
      IsActive = 1;
    } else {
      IsActive = 0;
    }
    let data = {
      CompanyId: "B02",
      UserName: this.userName,
      IsActive: IsActive,
    };
    let URL = "Account/AssociateDissociateUser";
    this.service.postData(data, URL).subscribe((data: any) => {
      if (data.Success) {
        this.appService.showMessage(data.Message, "X");
        this.dialog.closeAll();
        this.getUserDetailByUserName();
      } else {
        this.appService.showMessage(data.Message, "X");
      }
    });
  }
}
