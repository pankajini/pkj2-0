import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AssociateUserBasicDetailsComponent } from './associate-user-basic-details.component';

describe('AssociateUserBasicDetailsComponent', () => {
  let component: AssociateUserBasicDetailsComponent;
  let fixture: ComponentFixture<AssociateUserBasicDetailsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AssociateUserBasicDetailsComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AssociateUserBasicDetailsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
