import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { ViewUbtComponent } from './view-ubt.component';

describe('ViewUbtComponent', () => {
  let component: ViewUbtComponent;
  let fixture: ComponentFixture<ViewUbtComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ ViewUbtComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ViewUbtComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
