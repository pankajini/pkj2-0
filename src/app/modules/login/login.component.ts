import { Component, OnInit ,TemplateRef, ViewChild} from '@angular/core';
import { AppService } from '../../shared/service/app.service';
import { ModuleService } from '../module.service';
import { MatDialog } from '@angular/material/dialog';





@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  @ViewChild('callAPIDialog') callAPIDialog: TemplateRef<any>;
  businessTypes:any=[];
  companys:any=[];
  username:string;
  password:string;
  businessId:any;
  TransporterID:any;
  loadingTransporter:any;
  UnloadingTransporter:any;
  conformSuperAdminPassword:any;
  superAdminPassword:any;
  constructor(private appService:AppService,private service:ModuleService,private dialog: MatDialog) { 
  this.getBusines();
  
  }

  ngOnInit() {
   
    // this.logiinInService.getBusinessName().subscribe((data:any)=>{
    
    // })
  }
getBusines(){
  let url = 'BusinessTypeApi/GetBusinessTypeAndSuperAdminUserStatus_SF'
  this.service.getData('',url).subscribe((data: any) => {
    console.log(data.BusinessTypeMaster.length);
    if (data.BusinessTypeMaster.length != 0) {
     this.businessTypes = data.BusinessTypeMaster
    }
    if(data.IsExistsSuperAdmin == false){
      let dialogRef = this.dialog.open(this.callAPIDialog);
    }
  })
}


login(){
  console.log(this.superAdminPassword)
  let obj ={
    "username": this.username,
    "password" 	: this.password,
    "Type" 		 : "Admin",
    "grant_type":"password"
  }

  
  this.service.login(obj)
  
//   if(this.businessId){
//     this.logiinInService.login(this.username,this.passwrod).subscribe((data:any)=>{
//       localStorage.setItem('access_token',data.access_token)
//       localStorage.setItem('businessId',this.businessId)
//       let obj={
//         "CompanyId":this.businessId,
//          "Id":0
//       }
//xsthis.logiinInService.getAreaBusinessDetails(obj).subscribe((data:any)=>{
//          console.log(data);
      
//        this.TransporterID=data[0].Id;
//         console.log(this.TransporterID);
//       localStorage.setItem('TransporterID',this.TransporterID)
//        this.loadingTransporter=data[1].Id;
//      console.log(this.loadingTransporter);
//       localStorage.setItem('loadingTransporter',this.loadingTransporter)
//        this.UnloadingTransporter=data[2].Id;
//       console.log(this.UnloadingTransporter);
//       localStorage.setItem('UnloadingTransporter',this.UnloadingTransporter)
     
//       })
      // this.appService.navigate('/operation',{})
//     })
   
  
//   }else{
//     this.alertService.alert(AlertType.Error,"Please Select Bussines Name");
//   }
  
}
createSuperAdmin(){
  if(this.superAdminPassword == this.conformSuperAdminPassword){
  let obj ={
    "Password" 	: this.superAdminPassword,
  	"Type" 		 : "Admin"
  }
  let url = 'Account/Register'
  this.service.postData(obj,url).subscribe((data: any) => {
  if(data.Success == false){
    this.appService.showMessage(data.Errors[0], 'X');
  }else{
    alert('Success');
    this.dialog.closeAll();
  }
  })
  }else{
    this.appService.showMessage('Password and Conform Password not matched', 'X');
  }
}
}