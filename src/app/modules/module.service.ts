import { Injectable } from "@angular/core";
import { HttpClient, HttpHeaders, HttpParams } from "@angular/common/http";
import { environment } from "../../environments/environment";
import {
  CanDeactivate,
  ActivatedRouteSnapshot,
  RouterStateSnapshot,
} from "@angular/router";
import { ViewAndUpdateCompanyComponent } from "./company/view-and-update-company/view-and-update-company.component";
import { Observable } from "rxjs";
import { AppService } from "../shared/service/app.service";

@Injectable({
  providedIn: "root",
})
export class ModuleService
  implements CanDeactivate<ViewAndUpdateCompanyComponent> {
  private host = environment.API_END_POINT;
  // private url: string =  this.host+'MasterDataApi/GetMasterDataTree_SF';

  private url: string = "";

  constructor(private http: HttpClient, private appService: AppService) {}

  getData(data, url) {
    this.url = this.host + url;
    return this.http.get(this.url, data);
  }
  save(data) {
    return this.http.put(this.url, data);
  }
  login(obj) {
    let loginurl = "http://pankajini.sighttechsolution.in/oauth/token";
    let params = new HttpParams()
      .set("grant_type", obj.grant_type)
      .set("username", obj.username)
      .set("password", obj.password)
      .set("type", obj.Type);
    this.http.post(loginurl, params).subscribe((data: any) => {
      console.log(data);
      localStorage.setItem("userName", data["User Name"]);
      localStorage.setItem("access_token", data.access_token);
      this.appService.navigate("/operation", {});
      console.log(localStorage.getItem("userName"));
    });
  }
  conformlogin(obj) {
    let loginurl = "http://pankajini.sighttechsolution.in/oauth/token";
    let params = new HttpParams()
      .set("grant_type", obj.grant_type)
      .set("username", obj.username)
      .set("password", obj.password)
      .set("type", obj.Type);
    return this.http.post(loginurl, params);
  }
  postData(data, url) {
    console.log(url);
    if (url == "oauth/token") {
      this.host = environment.API_END_POINT_LOGIN;
      var reqHeader = new HttpHeaders({
        "Content-Type": "application/x-www-form-urlencoded",
        "No-Auth": "True",
      });
      return this.http.post(this.host + url, data, { headers: reqHeader });
    } else {
      this.host = environment.API_END_POINT;
    }
    this.url = this.host + url;

    return this.http.post(this.url, data);
  }
  getSingleData(data) {
    return this.http.get(this.url + "/" + data);
  }
  canDeactivate(
    component: ViewAndUpdateCompanyComponent
  ): Observable<boolean> | boolean {
    console.log(component);
    if (
      component.companyForm.myForm.touched ||
      component.documentsForm.files.length != 0 ||
      component.vendorForm.isChanged ||
      component.customerForm.isChanged
    ) {
      if (confirm("Do you want to save changes") == true)
        return component.saveChanges();
      else {
        return component.discardChanges();
      }
    } else {
      return true;
    }
  }
}
