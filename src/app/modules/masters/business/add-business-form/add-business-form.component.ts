import { Component, OnInit,Input, ViewChild,TemplateRef, Output, EventEmitter, SimpleChanges } from '@angular/core';
import {Business } from '../../../../shared/entities/business';
import { NgForm } from "@angular/forms";
import { Validators } from '../../../../shared/utils/validators';
import { MatDialog } from '@angular/material/dialog';
import { ModuleService } from '../../../module.service';
@Component({
  selector: 'app-add-business-form',
  templateUrl: './add-business-form.component.html',
  styleUrls: ['./add-business-form.component.css']
})
export class AddBusinessFormComponent implements OnInit {

  validators:any = Validators;
  @Input() business: Business = new Business();
  @ViewChild(NgForm, { static: true }) myForm: NgForm;
  @Input() industries:any=[];
  @Input() isEdit: boolean;
  @Output() saveData = new EventEmitter();
  @Output() getBusinessDetails =new EventEmitter();
  constructor() { }
  ngOnChanges(changes: SimpleChanges) {
console.log(this.industries)
  }
  ngOnInit() {
    
  }
  onSubmit(form) {
    this.saveData.emit(this.business);
    this.getBusinessDetails.emit(this.business);
  }
  ngOnDestroy(): void {
    

  }
}
