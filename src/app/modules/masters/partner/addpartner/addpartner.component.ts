import {
  Component,
  OnInit,
  Input,
  ViewChild,
  Output,
  EventEmitter,
  SimpleChanges,
  OnDestroy,
} from "@angular/core";
import { Router } from "@angular/router";
import { ResizeEvent } from "angular-resizable-element";
import { UBT } from "../../../ubt/ubt";
import { ModuleService } from "../../../module.service";
import { NgForm } from "@angular/forms";
import { Partner } from "../../../../shared/entities/partner";
import { AppService } from "../../../../shared/service/app.service";
import { DocumentFiles } from "../../../../shared/entities/document";
import { Bank } from "../../../../shared/entities/bank";
import { AppComponent } from "../../../../app.component";

@Component({
  selector: "app-addpartner",
  templateUrl: "./addpartner.component.html",
  styleUrls: ["./addpartner.component.css"],
})
export class AddpartnerComponent implements OnInit {
  height: any = 1;
  previousHeight: number = 43;
  innerHeight: number;
  width: number = 25;
  maxHeight: number;
  restoreHeight: number;
  selectedTab: any = 0;
  partner: Partner = new Partner();
  document: DocumentFiles = new DocumentFiles();
  isEdit: boolean = true;
  isEditTab: boolean = false;

  states: any = [];
  PartnerType: any = [];
  ubt: UBT = new UBT();

  panDocumentPath: any;
  tanDocumentPath: any;
  cinDocumentPath: any;
  gstinDocumentPath: any;
  constructor(
    private router: Router,
    private service: ModuleService,
    private appService: AppService,
    public appComponent: AppComponent
  ) {
    this.getStates();
    this.getPartnerType();
  }
  ngOnInit() {
    this.maxHeight = window.innerHeight - 56;
    if (localStorage.getItem("ubt")) {
      this.ubt = JSON.parse(localStorage.getItem("ubt"));
    }
    if (localStorage.getItem("ubtTab")) {
      this.selectedTab = localStorage.getItem("ubtTab");
    }
    if (localStorage.getItem("ubtHeight")) {
      this.height = localStorage.getItem("ubtHeight");
    } else {
      // this.height = 1;
      this.height = window.innerHeight / 2.2;
    }
  }
  back() {
    this.router.navigate(["/operation", {}]);
  }

  onResizeEnd(event: ResizeEvent) {
    if (event) {
      //  console.log(event.rectangle.height);
      if (
        event.rectangle.height >= 43 &&
        event.rectangle.height <= window.innerHeight - 56
      ) {
        this.height = event.rectangle.height;
        this.previousHeight = this.height;
      }
    }
  }
  minWidth() {
    this.width = 0;
  }
  maxWidth() {
    this.width = 25;
  }
  min() {
    this.height = 1;
  }
  max() {
    if (this.height != window.innerHeight - 56) {
      this.height = window.innerHeight - 56;
    } else {
      this.height = this.previousHeight;
    }
  }
  previous() {
    this.height = this.previousHeight;
  }
  restore() {
    this.height = window.innerHeight / 2.2;
    this.restoreHeight = window.innerHeight / 2.2;
  }
  tabChange(event) {
    //   console.log(event)
    this.selectedTab = event.index;
  }
  ngOnDestroy(): void {
    localStorage.setItem("ubt", JSON.stringify(this.ubt));
    if (this.selectedTab != 0) {
      localStorage.setItem("ubtTab", this.selectedTab);
    }
    localStorage.setItem("ubtHeight", this.height);
  }
  getStates() {
    let url = "MasterDataApi/GetAllStates_SF";
    this.service.getData({}, url).subscribe((data: any) => {
      console.log(data);
      this.states = data;
    });
  }
  getPartnerType() {
    let url = "MasterDataApi/GetPartnerType_SF";
    this.service.getData({}, url).subscribe((data: any) => {
      console.log(data);
      this.PartnerType = data;
    });
  }
  saveData($event) {
    // console.log($event);
    this.partner = $event;
    let url = "MasterDataApi/UpsertPartnerMaster";
    this.service.postData(this.partner, url).subscribe((data: any) => {
      console.log(data);
      if (data != null) {
        this.appService.showMessage("Saved Successfully", "X");

        //getPartnerDetailsById
        let obj = { PartnerId: data };
        console.log(obj);
        let url = "MasterDataApi/GetPartnerMaster_SF";
        this.service.postData(obj, url).subscribe((data: any) => {
          console.log(data);
          if (data.length != 0) {
            this.partner = null;
            this.partner = data[0];
            this.isEditTab = true;
          }
        });
        this.isEdit = true;
      } else {
        this.appService.showMessage("Somethimg went wrong", "X");
      }
    });
  }
  saveDocumentFiles($event) {
    this.document.EntityCategory = "Master";
    this.document.BusinessId = "";
    this.document.BusinessName = "";
    this.document.CompanyId = "";
    this.document.CompanyName = "";
    this.document.BusinesTypes = "";
    this.document.EntityParent = "";
    this.document.EntityType = "Partner";
    this.document.EntityName =
      this.partner.PartnerId + "_" + this.partner.PartnerName;
    this.document.DocumentType = $event.UploadedFileName;
    this.document.ControlId = "ControlId-1";
    this.document.FileDetails = $event;
    let url = "MasterDataApi/SaveDocument";
    let data = this.document;
    // console.log(data);
    this.service.postData(data, url).subscribe((data: any) => {
      console.log(data);
      if (data) {
        this.appService.showMessage("Saved Successfully", "X");
        this.getDocuments(this.document.DocumentType);
      } else {
        this.appService.showMessage("Somethimg went wrong", "X");
      }
    });
  }

  getDocuments(type) {
    let data = {
      DocumentInfo: [
        {
          EntityCategory: "Master",
          BusinessId: "",
          BusinessName: "",
          CompanyId: "",
          CompanyName: "",
          BusinesTypes: "",
          EntityParent: "",
          EntityType: "Partner",
          EntityName: this.partner.PartnerId + "_" + this.partner.PartnerName,
          DocumentType: type,
          ControlId: "ControlId-1",
          FilesCount: 1,
        },
      ],
    };
    let url = "FilesApi/GetAllFilesforScreen_SF";
    this.service.postData(data, url).subscribe((data: any) => {
      console.log(data);
      if (type == "PAN") {
        if (data && data.AbsolutePath[0].DocumentsAbsolutePath.length != 0) {
          this.panDocumentPath =
            data.AbsolutePath[0].DocumentsAbsolutePath[0].AbsoluteFilePath;
        } else {
          this.panDocumentPath = null;
        }
      } else if (type == "GSTIN") {
        if (data && data.AbsolutePath[0].DocumentsAbsolutePath.length != 0) {
          this.gstinDocumentPath =
            data.AbsolutePath[0].DocumentsAbsolutePath[0].AbsoluteFilePath;
        } else {
          this.gstinDocumentPath = null;
        }
      } else if (type == "TAN") {
        if (data && data.AbsolutePath[0].DocumentsAbsolutePath.length != 0) {
          this.tanDocumentPath =
            data.AbsolutePath[0].DocumentsAbsolutePath[0].AbsoluteFilePath;
        } else {
          this.tanDocumentPath = null;
        }
      } else if (type == "CIN") {
        if (data && data.AbsolutePath[0].DocumentsAbsolutePath.length != 0) {
          this.cinDocumentPath =
            data.AbsolutePath[0].DocumentsAbsolutePath[0].AbsoluteFilePath;
        } else {
          this.cinDocumentPath = null;
        }
      } else {
        this.panDocumentPath = null;
        this.gstinDocumentPath = null;
        this.tanDocumentPath = null;
        this.cinDocumentPath = null;
      }
    });
  }
}
