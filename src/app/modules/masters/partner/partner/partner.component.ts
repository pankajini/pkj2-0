import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
@Component({
  selector: 'app-partner',
  templateUrl: './partner.component.html',
  styleUrls: ['./partner.component.css']
})
export class PartnerComponent implements OnInit {
  constructor(private router: Router) { }

  ngOnInit() {
  }
  addPartner(){
    this.router.navigate(['/masters/partner/addpartner',{}])
  }
  viewPartner(){
    this.router.navigate(['/masters/partner/viewpartner',{}])
  }

}
