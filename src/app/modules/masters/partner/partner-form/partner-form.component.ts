import { Component, OnInit,Input, ViewChild,TemplateRef, Output, EventEmitter, SimpleChanges } from '@angular/core';
import {Partner } from '../../../../shared/entities/partner';
import { NgForm } from "@angular/forms";
import { Validators } from '../../../../shared/utils/validators';
import { MatDialog } from '@angular/material/dialog';
import { ModuleService } from '../../../module.service';
@Component({
  selector: 'app-partner-form',
  templateUrl: './partner-form.component.html',
  styleUrls: ['./partner-form.component.css']
})
export class PartnerFormComponent implements OnInit {
  validators:any = Validators;
  @Input() partner: Partner = new Partner();
  @ViewChild(NgForm, { static: true }) myForm: NgForm;
  @Input() dataLoad: any = [];
  @Input() partnerTypeLoad:any=[];
  @Input() customerTypesLoad: any = [];
  @Input() isEdit: boolean;
  @Output() saveData = new EventEmitter();

  PartnerFunctions:any=[{'Id':1,'PartnerFunctionType':'Vendor'},
                       {'Id':2,'PartnerFunctionType':'Customer'},
                       {'Id':3,'PartnerFunctionType':'Both'}]
  constructor() { }
  ngOnChanges(changes: SimpleChanges) {
console.log(this.partnerTypeLoad)
  }
  ngOnInit() {
    
  }
  onSubmit(form) {
    this.saveData.emit(this.partner)
  }
  ngOnDestroy(): void {
    

  }
}
