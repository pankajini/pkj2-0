import { Component, OnInit, TemplateRef } from "@angular/core";
import { NgForm } from "@angular/forms";
import { MatDialog } from "@angular/material/dialog";
import { ModuleService } from "../../../module.service";
import { AppService } from "../../../../shared/service/app.service";
import { Location } from "@angular/common";
import { Company } from "../../../../shared/entities/company";
import { Business } from "../../../../shared/entities/business";
import { AppComponent } from "../../../../app.component";
@Component({
  selector: "app-add-company",
  templateUrl: "./add-company.component.html",
  styleUrls: ["./add-company.component.css"],
})
export class AddCompanyComponent implements OnInit {
  company: Company = new Company();
  height: any = 1;
  previousHeight: number = 43;
  innerHeight: number;
  width: number = 19;
  maxHeight: number;
  restoreHeight: number;
  selectedTab: any = 0;

  dataSrc: any = [];
  isEdit: boolean = false;
  business: Business = new Business();

  isNodelLabelChange: boolean = false;
  childrenNode: any;
  status: any;
  BusinessTypeId: string;
  BusinessName: string;

  constructor(
    private dialog: MatDialog,
    private service: ModuleService,
    private appService: AppService,
    private location: Location,
    public appComponent: AppComponent
  ) {}

  ngOnInit() {
    this.maxHeight = window.innerHeight - 56;
    if (localStorage.getItem("ubt")) {
      //this.ubt = JSON.parse(localStorage.getItem('ubt'))
    }
    if (localStorage.getItem("ubtTab")) {
      this.selectedTab = localStorage.getItem("ubtTab");
    }
    if (localStorage.getItem("ubtHeight")) {
      this.height = localStorage.getItem("ubtHeight");
    } else {
      this.height = 1;
      this.height = window.innerHeight / 2.2;
    }
    this.getTreeData();
  }
  minWidth() {
    this.width = 0;
  }
  maxWidth() {
    this.width = 19;
  }
  min() {
    this.height = 1;
  }
  max() {
    if (this.height != window.innerHeight - 90) {
      this.height = window.innerHeight - 90;
    } else {
      this.height = this.previousHeight;
    }
  }
  previous() {
    this.height = this.previousHeight;
  }
  restore() {
    this.height = window.innerHeight / 2.2;
    this.restoreHeight = window.innerHeight / 2.2;
  }
  getTreeData() {
    let object = {
      SchemaName: "dbo",
      EntityCategory: "Master",
      Entity: "BUSINESSTYPE",
      ReturnType: "TREE",
    };
    let URL = "Tree/GetTree_SF";
    this.service.postData(object, URL).subscribe((data: any) => {
      console.log(data);
      this.dataSrc = data.RecursiveObjects;
      console.log(this.dataSrc);
    });
  }
  onSubmit(myForm) {
    this.dialog.closeAll();
  }
  refresh() {
    this.getTreeData();
  }
  saveData($event) {
    this.company = $event;
    this.createCompanyandAdminUser();
  }
  saveChanges() {}
  discardChanges() {}
  nodeLabel(node) {
    console.log(node);
    this.childrenNode = node.Id;
    this.BusinessName = node.Name;
    this.BusinessTypeId = node.Id;
  }
  dataNode(e) {}
  viewDetails() {}
  checkPermission() {}
  createCompanyandAdminUser() {
    let obj = {
      RegisterBindingModels: {
        BusinessTypeId: this.BusinessTypeId,
        BusinessName: this.BusinessName,
        Status: 1,
        Password: this.company["Password"],
      },
      Company: {
        CompanyName: this.company.CompanyName,
        OfficialEmailDomain: "RasmiTransport.com",
        Status: 1,
      },
    };
    let URL = "Account/CreateCompanyandAdminUser";
    this.service.postData(obj, URL).subscribe((data: any) => {
      console.log(data);
    });
  }
}
