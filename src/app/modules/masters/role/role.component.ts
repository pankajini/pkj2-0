import { Component, OnInit, ViewChild } from "@angular/core";
import { NgForm } from "@angular/forms";
import { AppService } from "src/app/shared/service/app.service";
import { ModuleService } from "../../module.service";
import { Role } from "./role";

@Component({
  selector: "app-role",
  templateUrl: "./role.component.html",
  styleUrls: ["./role.component.css"],
})
export class RoleComponent implements OnInit {
  role: Role = new Role();
  @ViewChild(NgForm, { static: true }) myForm: NgForm;
  constructor(private appService: AppService, private service: ModuleService) {}

  ngOnInit(): void {}
  onSubmit(event) {
    let url = "Account/RoleCreation";
    let arr = [];
    arr = this.role.Role.split("_");
    if (arr.length > 1) {
      if (arr[0] == "SA" || arr[0] == "CA" || arr[0] == "BU") {
        this.service.postData(this.role, url).subscribe((data: any) => {
          console.log(data);
          if (data.Success) {
            this.appService.showMessage(data.Errors[0], "X");
          } else {
            this.appService.showMessage(data.Errors[0], "X");
          }
        });
      } else {
        alert(arr[0]);
        this.appService.showMessage("Role must start with SA_,CA_,BU_", "X");
      }
    } else {
      this.appService.showMessage("Role must start with SA_,CA_,BU_", "X");
    }
  }
}
