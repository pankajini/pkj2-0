import { Injectable } from '@angular/core';
import {ViewCustomerDetailsComponent} from '../customer/view-customer-details/view-customer-details.component';
import {Observable} from "rxjs";
import { CanDeactivate} from "@angular/router";
@Injectable({
  providedIn: 'root'
})
export class CustomerService {

 
  constructor() { }
  canDeactivate(component: ViewCustomerDetailsComponent): Observable<boolean> | boolean{
    console.log(component)
  if(component.customerForm.myForm.touched
    || component.documentsForm.files.length != 0
    || component.agencyForm.isChanged
    || component.bankForm.myForm){
    
    if (confirm('Do you want to save changes') == true) 
       return component.saveChanges(); 
    else { 
        return component.discardChanges() 
  }
  }else{
    return true;
  }
}
}

