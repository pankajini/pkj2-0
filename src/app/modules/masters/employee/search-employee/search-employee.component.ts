import { Component, OnInit,ViewChild,Output,EventEmitter, Input } from '@angular/core';
import { SearchEmploye } from '../../../../shared/entities/employee';
import { NgForm } from "@angular/forms";
import { Validators } from '../../../../shared/utils/validators';
@Component({
  selector: 'app-search-employee',
  templateUrl: './search-employee.component.html',
  styleUrls: ['./search-employee.component.css']
})
export class SearchEmployeeComponent implements OnInit {
  @Input() searchEmployee:SearchEmploye= new SearchEmploye(); 
  @ViewChild(NgForm, { static: true }) myForm: NgForm;
 
  @Output() searchEmployees = new EventEmitter();
  constructor() { }

  ngOnInit() {
  }
  onSubmit(form) {
    this.searchEmployees.emit(this.searchEmployee);
  }
  ngOnDestroy(): void {
    

  }
}
