import { Component, OnInit,Input,SimpleChange, ViewChild } from '@angular/core';
import { NgForm } from "@angular/forms";
import { Bank } from '../../../../shared/entities/bank';
@Component({
  selector: 'app-employee-linguistic-proficiency',
  templateUrl: './employee-linguistic-proficiency.component.html',
  styleUrls: ['./employee-linguistic-proficiency.component.css']
})
export class EmployeeLinguisticProficiencyComponent implements OnInit {
  bankObj:Bank= new Bank();
  @Input() Languages:any;
  @Input() isEdit: boolean;
  @ViewChild(NgForm, { static: true }) myForm: NgForm;
  constructor() { }

  ngOnInit() {
  }
  ngOnChanges(changes:SimpleChange){
  
  }
  onSubmit(myForm){

  }
  openBankForm(myForm,data){

  }

}
