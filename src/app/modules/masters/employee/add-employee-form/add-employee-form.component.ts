import { Component, OnInit, Input, ViewChild,TemplateRef, Output, EventEmitter, SimpleChanges, OnDestroy, ɵConsole } from '@angular/core';
import { Employee } from '../../../../shared/entities/employee';
import { NgForm } from "@angular/forms";
import { Validators } from '../../../../shared/utils/validators';
import { MatDialog } from '@angular/material/dialog';
import { ModuleService } from '../../../module.service';
@Component({
  selector: 'app-add-employee-form',
  templateUrl: './add-employee-form.component.html',
  styleUrls: ['./add-employee-form.component.css']
})
export class AddEmployeeFormComponent implements OnInit {
  validators:any = Validators;
  @Input() employee: Employee = new Employee();
  @ViewChild(NgForm, { static: true }) myForm: NgForm;
  @Input() dataLoad: any = [];
  @Output() saveData = new EventEmitter();
  @Input() isEdit: boolean=true;
  Password:any;
  ConfirmPassword:any;
  CompanyName:any;

  BloodGroup:any=["A+", "B+", "B-", "O+", "O-","AB+", "AB-"];
  constructor(private dialog: MatDialog,private service:ModuleService) { }
  ngOnChanges(changes: SimpleChanges) {

  }
  ngOnInit() {
  // this.getBusines();
  }

  // getBusines(){
  //   let url = 'BusinessTypeApi/GetBusinessTypeAndSuperAdminUserStatus_SF'
  //   this.service.getData('',url).subscribe((data: any) => {
  //     console.log(data.BusinessTypeMaster.length);
  //     if (data.BusinessTypeMaster.length != 0) {
  //      this.businessTypes = data.BusinessTypeMaster;
  //      console.log(this.businessTypes);
  //     }
  //     if(data.IsExistsSuperAdmin == false){
  //      // let dialogRef = this.dialog.open(this.callAPIDialog);
  //     }
  //   })
  // }

  popWindow(templateRef: TemplateRef<any>){
    this.dialog.open(templateRef,{ disableClose: true });
  }
  onSubmit(form) {
    this.employee.DOB="01/01/1996";
    //console.log(this.employee);
    this.saveData.emit(this.employee)
  }
  ngOnDestroy(): void {
    

  }

  saveComapny(){
    console.log(this.Password);
    console.log(this.ConfirmPassword);
    let object={
      'CompanyName':this.CompanyName,
      'Password':this.Password
    }
    
    this.saveData.emit(object);
    this.dialog.closeAll();
  }
}
