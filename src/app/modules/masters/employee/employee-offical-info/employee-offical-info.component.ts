import { Component, OnInit, Input, ViewChild,TemplateRef, Output, EventEmitter, SimpleChanges, OnDestroy, ɵConsole } from '@angular/core';
import { EmployeeOfficalInfo } from '../../../../shared/entities/employee';
import { NgForm } from "@angular/forms";
import { Validators } from '../../../../shared/utils/validators';
import { MatDialog } from '@angular/material/dialog';
import { ModuleService } from '../../../module.service';
@Component({
  selector: 'app-employee-offical-info',
  templateUrl: './employee-offical-info.component.html',
  styleUrls: ['./employee-offical-info.component.css']
})
export class EmployeeOfficalInfoComponent implements OnInit {
  @Input() employeeOfficalInfo: EmployeeOfficalInfo = new EmployeeOfficalInfo();
  @ViewChild(NgForm, { static: true }) myForm: NgForm;
  @Input() employmentType:any=[{"EmploymentTypeId":0,"Name":"Permanent"}
                             ,{"EmploymentTypeId":1,"Name":"Contract"}]
  @Input() lineManagers:any=[];
  @Input() isEdit: boolean;
  @Output() saveEmployeeOfficalInfo = new EventEmitter();
  constructor() {
    console.log(this.lineManagers);
   }

  ngOnInit() {
    console.log(this.lineManagers);
  }
  onSubmit(form) {
  
    //console.log(this.employeeOfficalInfo);
    this.saveEmployeeOfficalInfo.emit(this.employeeOfficalInfo)
  }
}
