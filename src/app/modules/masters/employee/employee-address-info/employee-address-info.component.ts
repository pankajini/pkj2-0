import { Component, OnInit, Input, ViewChild,TemplateRef, Output, EventEmitter, SimpleChanges, OnDestroy, ɵConsole } from '@angular/core';
import { EmployeeAddressInfo } from '../../../../shared/entities/employee';
import { NgForm } from "@angular/forms";
import { Validators } from '../../../../shared/utils/validators';
import { MatDialog } from '@angular/material/dialog';
import { ModuleService } from '../../../module.service';
@Component({
  selector: 'app-employee-address-info',
  templateUrl: './employee-address-info.component.html',
  styleUrls: ['./employee-address-info.component.css']
})
export class EmployeeAddressInfoComponent implements OnInit {
  @Input() employeeAddressInfo: EmployeeAddressInfo = new EmployeeAddressInfo();
  @ViewChild(NgForm, { static: true }) myForm: NgForm;
  @Input() isEdit: boolean;
  @Input() dataLoad: any = [];
  @Output() saveEmployeeAddressInfo = new EventEmitter();
  constructor() { }

  ngOnInit() {
  }
  onSubmit(form) {
  
    //console.log(this.employeeAddressInfo);
    this.saveEmployeeAddressInfo.emit(this.employeeAddressInfo)
  }
}
