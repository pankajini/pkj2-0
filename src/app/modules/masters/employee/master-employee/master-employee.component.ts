import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
@Component({
  selector: 'app-master-employee',
  templateUrl: './master-employee.component.html',
  styleUrls: ['./master-employee.component.css']
})
export class MasterEmployeeComponent implements OnInit {
  constructor(private router: Router) { }

  ngOnInit() {
  }
  addEmployee(){
    this.router.navigate(['/masters/employee/addemployee',{}])
  }
  viewEmployee(){
    this.router.navigate(['/masters/employee/viewemployee',{}])
  }

}
