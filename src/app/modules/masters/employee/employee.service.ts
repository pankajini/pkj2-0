import { Injectable } from '@angular/core';
import {ViewEmployeeComponent} from './view-employee/view-employee.component';
import {Observable} from "rxjs";
@Injectable({
  providedIn: 'root'
})
export class EmployeeService {

  constructor() { }
  canDeactivate(component: ViewEmployeeComponent): Observable<boolean> | boolean{
    console.log(component)
  if(component.addEmployeForm.myForm.touched
     || component.employeOfficalInfoForm.myForm.touched
     ||component.employeeAddressInfoForm.myForm.touched
     || component.employeeContactInfoForm.myForm.touched
     || component.employeeEmergencyInfoForm.myForm.touched){
    if (confirm('Do you want to save changes') == true) 
       return component.saveChanges(); 
    else { 
        return component.discardChanges() 
  }
  }else{
    return true;
  }
}
}
