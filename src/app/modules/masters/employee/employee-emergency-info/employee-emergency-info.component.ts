import { Component, OnInit, Input, ViewChild,TemplateRef, Output, EventEmitter, SimpleChanges, OnDestroy, ɵConsole } from '@angular/core';
import {EmployeeEmergencyInfo} from '../../../../shared/entities/employee';
import { NgForm } from "@angular/forms";
import { Validators } from '../../../../shared/utils/validators';
import { MatDialog } from '@angular/material/dialog';
import { ModuleService } from '../../../module.service';
@Component({
  selector: 'app-employee-emergency-info',
  templateUrl: './employee-emergency-info.component.html',
  styleUrls: ['./employee-emergency-info.component.css']
})
export class EmployeeEmergencyInfoComponent implements OnInit {
  @Input() employeeEmergencyInfo: EmployeeEmergencyInfo = new EmployeeEmergencyInfo();
  @ViewChild(NgForm, { static: true }) myForm: NgForm;
  @Input() isEdit: boolean;

  @Output() saveEmployeeEmergencyInfo = new EventEmitter();
  constructor() { }

  ngOnInit() {
  }
  onSubmit(form) {
  
    //console.log(this.employeeAddressInfo);
    this.saveEmployeeEmergencyInfo.emit(this.employeeEmergencyInfo);
  }
}
