import { Component, OnInit, Input, ViewChild,TemplateRef, Output, EventEmitter, SimpleChanges, OnDestroy, ɵConsole } from '@angular/core';
import { EmployeeContactInfo } from '../../../../shared/entities/employee';
import { NgForm } from "@angular/forms";
import { Validators } from '../../../../shared/utils/validators';
import { MatDialog } from '@angular/material/dialog';
import { ModuleService } from '../../../module.service';
@Component({
  selector: 'app-employee-contact-info',
  templateUrl: './employee-contact-info.component.html',
  styleUrls: ['./employee-contact-info.component.css']
})
export class EmployeeContactInfoComponent implements OnInit {
  @Input() employeeContactInfo: EmployeeContactInfo = new EmployeeContactInfo();
  @ViewChild(NgForm, { static: true }) myForm: NgForm;
  @Input() isEdit: boolean;

  @Output() saveEmployeeContactInfo = new EventEmitter();
  constructor() { }

  ngOnInit() {
  }
  onSubmit(form) {
  
    //console.log(this.employeeAddressInfo);
    this.saveEmployeeContactInfo.emit(this.employeeContactInfo)
  }
}
