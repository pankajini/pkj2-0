import { Component, OnInit, Input, ViewChild,TemplateRef, Output, EventEmitter, SimpleChanges, OnDestroy, ɵConsole } from '@angular/core';
import {EmployeeDocumentsInfo} from '../../../../shared/entities/employee';
import { NgForm } from "@angular/forms";
import { FileDetails } from '../../../../shared/entities/document';
import { MatDialog } from '@angular/material/dialog';
import { DomSanitizer} from '@angular/platform-browser';
@Component({
  selector: 'app-employee-documents',
  templateUrl: './employee-documents.component.html',
  styleUrls: ['./employee-documents.component.css']
})
export class EmployeeDocumentsComponent implements OnInit {
  @Input() employeeDocumentsInfo: EmployeeDocumentsInfo = new EmployeeDocumentsInfo();
  @ViewChild(NgForm, { static: true }) myForm: NgForm;
  @Input() isEdit: boolean;
  @Output() saveEmployeeDocumentsInfo = new EventEmitter();
  filesDetails: FileDetails = new FileDetails();
  pdfPath:any;
  files:any=[];

  @Input() adhar: any;
  @Input() voterId:any;
  @Input() pan:any;
  @Input() passport:any;
  @Input() photograph:any;
  @Input() highestQualification:any;
  @Input() yearOfCompletion:any;

  isEditAdhar:boolean=true;
  isEditVoterId:boolean=true;
  isEditPan:boolean=true;
  isEditPassport:boolean=true;
  isEditPhotograph:boolean=true;
  isEditHighestQualification:boolean=true;
  isEditYearOfCompletion:boolean=true;

  constructor(private dialog: MatDialog,private sanitizer: DomSanitizer) { }
  ngOnChanges(changes: SimpleChanges) {
    if (this.adhar) {
      this.isEditAdhar = false
    }else{
      this.isEditAdhar = true
    }

    if(this.voterId){
      this.isEditVoterId = false
    }else{
      this.isEditVoterId = true
    }

    if(this.pan){
      this.isEditPan = false
    }else{
      this.isEditPan = true
    }

    if(this.passport){
      this.isEditPassport = false
    }else{
      this.isEditPassport = true
    }

    if(this.photograph){
      this.isEditPhotograph =false
    }
    else{
      this.isEditPhotograph=true;
    }

    if(this.highestQualification){
      this.isEditHighestQualification=false;
    }
    else{
      this.isEditHighestQualification=true;
    }

    if(this.yearOfCompletion){
      this.isEditYearOfCompletion=false;
    }
    else{
      this.isEditYearOfCompletion=true;
    }

  }
  ngOnInit() {
  }
  handleFileSelect(evt) {
    
   // this.filesDetails.UploadedFileName = name;
    var File = evt.target.value;
    let subStringData = File.substr(12);
    var FileName = subStringData.split('.')[0];
    var FileType = subStringData.split('.')[1];

    this.filesDetails.FilePath = FileName;
    this.filesDetails.FileExtn = FileType;
    var files = evt.target.files;
    var file = files[0];
    if (files && file) {
      var reader = new FileReader();
      reader.onload = this._handleReaderLoaded.bind(this);
      reader.readAsBinaryString(file);
    }
  }

  _handleReaderLoaded(readerEvt) {
    var binaryString = readerEvt.target.result;

    this.filesDetails.EncryptedFile = btoa(binaryString);
    //  console.log(this.filesDetails.EncryptedFile)    
    let obj = {...this.filesDetails}
    this.files.push(obj);
  }
  save(event){
    this.filesDetails.UploadedFileName =event;
    this.saveEmployeeDocumentsInfo.emit(this.filesDetails);
  }
  viewPdf(templateRef: TemplateRef<any>,path) {
 
    this.dialog.open(templateRef);
    this.pdfPath = this.sanitizer.bypassSecurityTrustResourceUrl(path);
  }
}
