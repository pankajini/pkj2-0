import { Component, OnInit, ViewChild, TemplateRef } from "@angular/core";
import { Employee, SearchEmploye } from "../../../../shared/entities/employee";
import { ModuleService } from "../../../module.service";
import { AppService } from "../../../../shared/service/app.service";
import { ResizeEvent } from "angular-resizable-element";

import { AddEmployeeFormComponent } from "../add-employee-form/add-employee-form.component";
import { EmployeeOfficalInfoComponent } from "../employee-offical-info/employee-offical-info.component";
import { EmployeeAddressInfoComponent } from "../employee-address-info/employee-address-info.component";
import { EmployeeContactInfoComponent } from "../employee-contact-info/employee-contact-info.component";
import { EmployeeEmergencyInfoComponent } from "../employee-emergency-info/employee-emergency-info.component";

import { MatDialog } from "@angular/material/dialog";
import { EmployeeOfficalInfo } from "../../../../shared/entities/employee";
import { EmployeeAddressInfo } from "../../../../shared/entities/employee";
import { EmployeeContactInfo } from "../../../../shared/entities/employee";
import { EmployeeEmergencyInfo } from "../../../../shared/entities/employee";
import { DocumentFiles } from "../../../../shared/entities/document";
import { AppComponent } from "../../../../app.component";
import { from } from "rxjs";
@Component({
  selector: "app-view-employee",
  templateUrl: "./view-employee.component.html",
  styleUrls: ["./view-employee.component.css"],
})
export class ViewEmployeeComponent implements OnInit {
  @ViewChild(AddEmployeeFormComponent) addEmployeForm: AddEmployeeFormComponent;
  @ViewChild(EmployeeOfficalInfoComponent)
  employeOfficalInfoForm: EmployeeOfficalInfoComponent;
  @ViewChild(EmployeeAddressInfoComponent)
  employeeAddressInfoForm: EmployeeAddressInfoComponent;
  @ViewChild(EmployeeContactInfoComponent)
  employeeContactInfoForm: EmployeeContactInfoComponent;
  @ViewChild(EmployeeEmergencyInfoComponent)
  employeeEmergencyInfoForm: EmployeeEmergencyInfoComponent;
  @ViewChild("statusDialog", { static: true }) statusDialog: TemplateRef<any>;

  height: any = 1;
  previousHeight: number = 43;
  innerHeight: number;
  width: number = 25;
  maxHeight: number;
  restoreHeight: number;
  selectedTab: any = 0;

  dataSrc: any = [];
  isEdit: boolean = false;
  employee: Employee = new Employee();
  employeeOfficalInfo: EmployeeOfficalInfo = new EmployeeOfficalInfo();
  employeeAddressInfo: EmployeeAddressInfo = new EmployeeAddressInfo();
  employeeContactInfo: EmployeeContactInfo = new EmployeeContactInfo();
  employeeEmergencyInfo: EmployeeEmergencyInfo = new EmployeeEmergencyInfo();
  document: DocumentFiles = new DocumentFiles();

  employeObject: any = {};

  currentEmployeId: any;
  states: any = [];
  adharDocumentPath: any;
  voterIdDocumentPath: any;
  panDocumentPath: any;
  passportDocumentPath: any;
  PhotographDocumentPath: any;
  highestQualification: any;
  YearOfCompletion: any;

  isNodelLabelChange: boolean = false;
  childrenNode: any;
  status: any;
  isCheckForm: boolean = false;

  lineManagers: any = [];
  languages: any = [];
  searchEmployeeObj: SearchEmploye = new SearchEmploye();
  constructor(
    private service: ModuleService,
    private appService: AppService,
    private dialog: MatDialog,
    public appComponent: AppComponent
  ) {}

  ngOnInit() {
    this.maxHeight = window.innerHeight - 56;
    if (localStorage.getItem("ubt")) {
      //this.ubt = JSON.parse(localStorage.getItem('ubt'))
    }
    if (localStorage.getItem("ubtTab")) {
      this.selectedTab = localStorage.getItem("ubtTab");
    }
    if (localStorage.getItem("ubtHeight")) {
      this.height = localStorage.getItem("ubtHeight");
    } else {
      this.height = 1;
      this.height = window.innerHeight / 2.2;
    }
  }
  onResizeEnd(event: ResizeEvent) {
    if (event) {
      //  sdfsdf(event.rectangle.height);
      if (
        event.rectangle.height >= 43 &&
        event.rectangle.height <= window.innerHeight - 56
      ) {
        this.height = event.rectangle.height;
        this.previousHeight = this.height;
      }
    }
  }
  minWidth() {
    this.width = 0;
  }
  maxWidth() {
    this.width = 19;
  }
  min() {
    this.height = 1;
  }
  max() {
    if (this.height != window.innerHeight - 90) {
      this.height = window.innerHeight - 90;
    } else {
      this.height = this.previousHeight;
    }
  }
  previous() {
    this.height = this.previousHeight;
  }
  restore() {
    this.height = window.innerHeight / 2.2;
    this.restoreHeight = window.innerHeight / 2.2;
  }

  searchEmployees($event) {
    let object = {
      EmployeeId: $event.EmployeeId,
      FirstName: $event.FirstName,
      LastName: $event.LastName,
      ReturnType: "FLAT",
    };

    this.searchEmployeeObj = $event;
    let URL = "EmployeeMasterApi/SearchEmployees_SF";
    this.service.postData(object, URL).subscribe((data: any) => {
      this.dataSrc = data.FlatObjects;
    });
  }

  nodeLabel(node) {
    // this.isEdit = true;

    this.isNodelLabelChange = true;
    if (node) {
      localStorage.setItem("nodeLabel", node);
      this.childrenNode = node;
    }
    if (
      this.addEmployeForm.myForm.touched ||
      this.employeOfficalInfoForm.myForm.touched ||
      this.employeeAddressInfoForm.myForm.touched ||
      this.employeeContactInfoForm.myForm.touched ||
      this.employeeEmergencyInfoForm.myForm.touched
    ) {
      this.openDialog();
    } else {
      this.changeNode();
    }
  }

  openDialog() {
    this.isCheckForm = true;
    this.dialog.open(this.statusDialog, { disableClose: true });
  }
  changeNode() {
    // this.releaseLock();
    this.releaseLock();
    this.getlineManager();
    this.getEmployeDetailsById();
  }
  getEmployeDetailsById() {
    //sdfsdf("hi called");
    this.getStates();

    this.getlanguages();
    let obj = {
      EmployeeId: this.childrenNode.Id,
    };
    let url = "EmployeeMasterApi/GetEmployeeDetailsByEmployeeId_SF";
    this.service.postData(obj, url).subscribe((data: any) => {
      if (data.length != 0) {
        this.employee = data[0];
        this.currentEmployeId = this.employee.EmployeeId;
        this.employeObject = data[0];

        // delete this.employeObject.CommonUserId;
        // delete this.employeObject.RecentPhotograph;
        // delete this.employeObject.CreatedOn;
        // delete this.employeObject.IsBlock;
        // delete this.employeObject.BlockReason;
        // delete this.employeObject.OfficeEmailDomain;
        // delete this.employeObject.ReturnType;

        this.employeeOfficalInfo.DateOfJoining = this.employeObject.DateOfJoining;
        this.employeeOfficalInfo.DateOfLeaving = this.employeObject.DateOfLeaving;
        if (this.employeObject.EmploymentType == "Permanent") {
          this.employeeOfficalInfo.EmploymentTypeId = 0;
        }
        if (this.employeObject.EmploymentType == "Contract") {
          this.employeeOfficalInfo.EmploymentTypeId = 1;
        }
        this.employeeOfficalInfo.ReasonOfLeaving = this.employeObject.ReasonOfLeaving;
        //this.employeeOfficalInfo.EmployeeId = 'E10001'

        this.employeeAddressInfo.HouseNo = this.employeObject.HouseNo;

        var str = this.employeObject.Address;
        if (this.employeObject.Address) {
          var words = str.split(" ");
          for (var i = 0; i < words.length - 1; i++) {
            words[i];
          }

          this.employeeAddressInfo.AddressLine1 = words[0];
          this.employeeAddressInfo.AddressLine2 = words[1];
          this.employeeAddressInfo.AddressLine3 = words[2];
        }

        this.employeeAddressInfo.LandMark = this.employeObject.LandMark;
        this.employeeAddressInfo.PostOffice = this.employeObject.PostOffice;
        this.employeeAddressInfo.PoliceStation = this.employeObject.PoliceStation;
        this.employeeAddressInfo.City = this.employeObject.City;
        this.employeeAddressInfo.District = this.employeObject.District;
        this.employeeAddressInfo.Pin = this.employeObject.Pin;
        this.employeeAddressInfo.StateId = parseInt(this.employeObject.State);

        this.employeeContactInfo.Phone1 = this.employeObject.Phone1;
        this.employeeContactInfo.Phone2 = this.employeObject.Phone2;
        this.employeeContactInfo.PersonalEmail = this.employeObject.PersonalEmail;

        this.employeeEmergencyInfo.EmergencyContactName = this.employeObject.EmergencyName;
        this.employeeEmergencyInfo.EmergencyContactPhone1 = this.employeObject.EmergencyPhone1;
        this.employeeEmergencyInfo.EmergencyContactPhone2 = this.employeObject.EmergencyPhone2;
        this.employeeEmergencyInfo.EmergencyContactEmail1 = this.employeObject.EmergencyEmail1;
        this.employeeEmergencyInfo.EmergencyContactEmail2 = this.employeObject.EmergencyEmail2;
        this.employeeEmergencyInfo.EmergencyContactRelationship = this.employeObject.EmergencyRelationship;
        this.employeeEmergencyInfo.EmergencyDependency = this.employeObject.EmergencyDependency;
        this.employeeEmergencyInfo.EmergencyContactOrganization = this.employeObject.EmergencyContactOrganization;

        this.getDocuments("ADHAR");
        this.getDocuments("VOTER");
        this.getDocuments("PAN");
        this.getDocuments("PASSPORT");
        this.getDocuments("PHOTOGRAPHDATE");
        this.getDocuments("HIGHESTQUALIFICATION");
      }
    });
  }
  getStates() {
    let url = "MasterDataApi/GetAllStates_SF";
    this.service.getData({}, url).subscribe((data: any) => {
      this.states = data;
    });
  }
  tabChange(event) {
    // sdfsdf(event)
    this.selectedTab = event.index;
  }
  getlineManager() {
    let URL = "EmployeeMasterApi/GetAllEmployeesNameAndId_SF";
    this.service.getData({}, URL).subscribe((data: any) => {
      this.lineManagers = data;
      //sdfsdf(this.dataSrc);
    });
  }
  getlanguages() {
    let url = "LanguageApi/GetAllLanguages_SF";
    this.service.getData({}, url).subscribe((data: any) => {
      this.languages = data;
    });
  }
  releaseLock() {
    let url = "ManageTransactionLockApi/ReleaseContextLock";
    let obj = {
      Type: "Master",
      LockContextType: "Employe",
      LockContextValue: this.currentEmployeId,
      UserID: "A01_Administrator",
    };

    this.service.postData(obj, url).subscribe((data: any) => {
      if ((data = "Released")) {
        this.isEdit = false;
      } else {
        //this.isEdit=true;
      }
    });
  }
  checkPermission() {
    this.releaseLock();
    let url = "ManageTransactionLockApi/SetContextLock";
    let obj = {
      Type: "Master",
      LockContextType: "Employe",
      LockContextValue: this.currentEmployeId,
      UserID: "A01_Administrator",
    };

    this.service.postData(obj, url).subscribe((data: any) => {
      this.isEdit = data.Status;
    });
  }
  viewDetails() {
    //if (this.agencyForm.myForm.touched) {
    //this.openDialog();
    //}
    // else {
    this.isEdit = false;
    this.releaseLock();
    // }
  }
  saveData($event) {
    //$event["IsActive"] = 1;
    this.employeObject.Action = "UPDATE";
    this.employeObject.IsActive = 1;

    (this.employeObject.EmployeeId = $event.EmployeeId),
      (this.employeObject.FirstName = $event.FirstName),
      (this.employeObject.MiddleName = $event.MiddleName),
      (this.employeObject.LastName = $event.LastName),
      (this.employeObject.DOB = $event.DOB),
      (this.employeObject.FatherName = $event.FatherName),
      (this.employeObject.MotherName = $event.MotherName),
      (this.employeObject.Gender = $event.Gender),
      (this.employeObject.Religion = $event.Religion),
      (this.employeObject.Nationality = $event.Nationality),
      (this.employeObject.BloodGroup = $event.BloodGroup),
      (this.employeObject.IdentityMark = $event.IdentityMark),
      (this.employeObject.MaritalStatus = $event.MaritalStatus),
      (this.employeObject.CreatedBy = $event.CreatedBy),
      (this.employeObject.Remarks = $event.Remarks);

    let url = "EmployeeMasterApi/UpdateEmployeeMaster";
    this.service.postData(this.employeObject, url).subscribe((data: any) => {
      if (
        data.ReturnStatusMessage == "Employee details updated successfully."
      ) {
        this.appService.showMessage(data.ReturnStatusMessage, "X");
        this.addEmployeForm.myForm.reset();
        this.isEdit = false;
      } else {
        this.appService.showMessage("Somethimg went wrong", "X");
        this.addEmployeForm.myForm.reset();
        this.isEdit = false;
      }
    });
  }
  saveEmployeeOfficalInfo($event) {
    if ($event.EmploymentTypeId == 0) {
      this.employeObject.EmploymentType = "Permanent";
    }
    if ($event.EmploymentTypeId == 1) {
      this.employeObject.EmploymentType = "Contract";
    }
    //this.employeObject.EmploymentType = $event.EmploymentType
    this.employeObject.DateOfJoining = $event.DateOfJoining;
    this.employeObject.DateOfLeaving = $event.DateOfLeaving;
    this.employeObject.ReasonOfLeaving = $event.ReasonOfLeaving;
    this.employeObject.LineManager = $event.LineManager;
    this.employeObject.Action = "UPDATE";
    this.employeObject.IsActive = 1;

    //sdfsdf(this.employeObject);

    let url = "EmployeeMasterApi/UpdateEmployeeMaster";
    this.service.postData(this.employeObject, url).subscribe((data: any) => {
      if (
        data.ReturnStatusMessage == "Employee details updated successfully."
      ) {
        this.appService.showMessage(data.ReturnStatusMessage, "X");
      } else {
        this.appService.showMessage("Somethimg went wrong", "X");
      }
    });
    this.getEmployeDetailsById();
  }
  saveEmployeeAddressInfo($event) {
    //sdfsdf($event);
    this.employeObject.HouseNo = $event.HouseNo;
    this.employeObject.Address =
      $event.AddressLine1 +
      " " +
      $event.AddressLine2 +
      " " +
      $event.AddressLine3;
    this.employeObject.LandMark = $event.LandMark;
    this.employeObject.PostOffice = $event.PostOffice;
    this.employeObject.PoliceStation = $event.PoliceStation;
    this.employeObject.City = $event.City;
    this.employeObject.District = $event.District;
    this.employeObject.Pin = $event.Pin;
    this.employeObject.State = $event.State;

    this.employeObject.Action = "UPDATE";
    this.employeObject.IsActive = 1;
    //  sdfsdf(this.employeObject);
    let url = "EmployeeMasterApi/UpdateEmployeeMaster";
    this.service.postData(this.employeObject, url).subscribe((data: any) => {
      if (
        data.ReturnStatusMessage == "Employee details updated successfully."
      ) {
        this.appService.showMessage(data.ReturnStatusMessage, "X");
      } else {
        this.appService.showMessage("Somethimg went wrong", "X");
      }
    });
  }
  saveEmployeeContactInfo($event) {
    //sdfsdf($event);
    this.employeObject.Phone1 = $event.Phone1;
    this.employeObject.Phone2 = $event.Phone2;
    this.employeObject.PersonalEmail = $event.PersonalEmail;

    this.employeObject.Action = "UPDATE";
    this.employeObject.IsActive = 1;
    //  sdfsdf(this.employeObject);
    let url = "EmployeeMasterApi/UpdateEmployeeMaster";
    this.service.postData(this.employeObject, url).subscribe((data: any) => {
      if (
        data.ReturnStatusMessage == "Employee details updated successfully."
      ) {
        this.appService.showMessage(data.ReturnStatusMessage, "X");
      } else {
        this.appService.showMessage("Somethimg went wrong", "X");
      }
    });
  }
  saveEmployeeEmergencyInfo($event) {
    // sdfsdf($event);
    this.employeObject.EmergencyName = $event.EmergencyContactName;
    this.employeObject.EmergencyPhone1 = $event.EmergencyContactPhone1;
    this.employeObject.EmergencyPhone2 = $event.EmergencyContactPhone2;
    this.employeObject.EmergencyEmail1 = $event.EmergencyContactEmail1;
    this.employeObject.EmergencyEmail2 = $event.EmergencyContactEmail2;
    this.employeObject.EmergencyRelationship =
      $event.EmergencyContactRelationship;
    this.employeObject.EmergencyDependency = $event.EmergencyDependency;
    this.employeObject.EmergencyContactOrganization =
      $event.EmergencyContactOrganization;

    this.employeObject.Action = "UPDATE";
    this.employeObject.IsActive = 1;
    //  sdfsdf(this.employeObject);
    let url = "EmployeeMasterApi/UpdateEmployeeMaster";
    this.service.postData(this.employeObject, url).subscribe((data: any) => {
      if (
        data.ReturnStatusMessage == "Employee details updated successfully."
      ) {
        this.appService.showMessage(data.ReturnStatusMessage, "X");
      } else {
        this.appService.showMessage("Somethimg went wrong", "X");
      }
    });
  }
  saveEmployeeDocumentsInfo($event) {
    //sdfsdf($event);
    this.document.EntityCategory = "Master";
    this.document.BusinessId = "";
    this.document.BusinessName = "";
    this.document.CompanyId = "";
    this.document.CompanyName = "";
    this.document.BusinesTypes = "";
    this.document.EntityParent = "";
    this.document.EntityType = "Employees";
    this.document.EntityName =
      this.employeObject.EmployeeId + "_" + this.employeObject.EmployeeName;
    this.document.DocumentType = $event.UploadedFileName;
    this.document.ControlId = "ControlId-1";
    this.document.FileDetails = $event;
    let url = "MasterDataApi/SaveDocument";
    let data = this.document;

    this.service.postData(data, url).subscribe((data: any) => {
      if (data) {
        this.appService.showMessage("Saved Successfully", "X");
        this.getDocuments(this.document.DocumentType);
        //this.closeDialog();
      } else {
        this.appService.showMessage("Somethimg went wrong", "X");
      }
    });
  }
  getDocuments(type) {
    let data = {
      DocumentInfo: [
        {
          EntityCategory: "Master",
          BusinessId: "",
          BusinessName: "",
          CompanyId: "",
          CompanyName: "",
          BusinesTypes: "",
          EntityParent: "",
          EntityType: "Employees",
          EntityName:
            this.employeObject.EmployeeId +
            "_" +
            this.employeObject.EmployeeName,
          DocumentType: type,
          ControlId: "ControlId-1",
          FilesCount: 1,
        },
      ],
    };
    let url = "FilesApi/GetAllFilesforScreen_SF";
    this.service.postData(data, url).subscribe((data: any) => {
      if (type == "ADHAR") {
        if (data && data.AbsolutePath[0].DocumentsAbsolutePath.length != 0) {
          this.adharDocumentPath =
            data.AbsolutePath[0].DocumentsAbsolutePath[0].AbsoluteFilePath;
        } else {
          this.adharDocumentPath = null;
        }
      } else if (type == "VOTERID") {
        if (data && data.AbsolutePath[0].DocumentsAbsolutePath.length != 0) {
          this.voterIdDocumentPath =
            data.AbsolutePath[0].DocumentsAbsolutePath[0].AbsoluteFilePath;
        } else {
          this.voterIdDocumentPath = null;
        }
      } else if (type == "PAN") {
        if (data && data.AbsolutePath[0].DocumentsAbsolutePath.length != 0) {
          this.panDocumentPath =
            data.AbsolutePath[0].DocumentsAbsolutePath[0].AbsoluteFilePath;
        } else {
          this.panDocumentPath = null;
        }
      } else if (type == "PASSPORT") {
        if (data && data.AbsolutePath[0].DocumentsAbsolutePath.length != 0) {
          this.passportDocumentPath =
            data.AbsolutePath[0].DocumentsAbsolutePath[0].AbsoluteFilePath;
        } else {
          this.passportDocumentPath = null;
        }
      } else if (type == "PHOTOGRAPH") {
        if (data && data.AbsolutePath[0].DocumentsAbsolutePath.length != 0) {
          this.PhotographDocumentPath =
            data.AbsolutePath[0].DocumentsAbsolutePath[0].AbsoluteFilePath;
        } else {
          this.PhotographDocumentPath = null;
        }
      } else if (type == "HIGHESTQUALIFICATION") {
        if (data && data.AbsolutePath[0].DocumentsAbsolutePath.length != 0) {
          this.highestQualification =
            data.AbsolutePath[0].DocumentsAbsolutePath[0].AbsoluteFilePath;
        } else {
          this.highestQualification = null;
        }
      } else if (type == "YEAROFCOMPLETION") {
        if (data && data.AbsolutePath[0].DocumentsAbsolutePath.length != 0) {
          this.YearOfCompletion =
            data.AbsolutePath[0].DocumentsAbsolutePath[0].AbsoluteFilePath;
        } else {
          this.YearOfCompletion = null;
        }
      } else {
        this.adharDocumentPath = null;
        this.voterIdDocumentPath = null;
        this.panDocumentPath = null;
        this.passportDocumentPath = null;
        this.PhotographDocumentPath = null;
        this.highestQualification = null;
        this.YearOfCompletion = null;
      }
    });
  }

  saveChanges() {
    if (this.addEmployeForm.myForm.touched) {
      this.saveData(this.employee);
      this.closeDialog();
      this.isEdit = false;
      this.addEmployeForm.myForm.reset();
      return true;
    } else if (this.employeOfficalInfoForm.myForm.touched) {
      this.saveEmployeeOfficalInfo(this.employeeOfficalInfo);
      this.closeDialog();
      this.isEdit = false;
      this.employeOfficalInfoForm.myForm.reset();
      return true;
    } else if (this.employeeAddressInfoForm.myForm.touched) {
      this.saveEmployeeAddressInfo(this.employeeAddressInfo);
      this.closeDialog();
      this.isEdit = false;
      this.employeeAddressInfoForm.myForm.reset();
      return true;
    } else if (this.employeeContactInfoForm.myForm.touched) {
      this.saveEmployeeContactInfo(this.employeeContactInfo);
      this.closeDialog();
      this.isEdit = false;
      this.employeeContactInfoForm.myForm.reset();
      return true;
    } else if (this.employeeEmergencyInfoForm.myForm.touched) {
      this.saveEmployeeEmergencyInfo(this.employeeEmergencyInfo);
      this.closeDialog();
      this.isEdit = false;
      this.employeeEmergencyInfoForm.myForm.reset();
      return true;
    }
  }
  closeDialog() {
    this.dialog.closeAll();
  }
  discardChanges() {
    if (
      this.addEmployeForm.myForm.touched ||
      this.employeOfficalInfoForm.myForm.touched ||
      this.employeeAddressInfoForm.myForm.touched ||
      this.employeeContactInfoForm.myForm.touched ||
      this.employeeEmergencyInfoForm.myForm.touched
    ) {
      this.addEmployeForm.myForm.reset();
      this.employeOfficalInfoForm.myForm.reset();
      this.employeeAddressInfoForm.myForm.reset();
      this.employeeContactInfoForm.myForm.reset();
      this.employeeEmergencyInfoForm.myForm.reset();

      this.getEmployeDetailsById();
      this.closeDialog();
      return true;
    }
  }
  dataNode(event) {}
}
