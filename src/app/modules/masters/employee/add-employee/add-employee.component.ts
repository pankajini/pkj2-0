import { Component, OnInit, TemplateRef } from "@angular/core";
import { NgForm } from "@angular/forms";
import { MatDialog } from "@angular/material/dialog";
import { ModuleService } from "../../../module.service";
import { AppService } from "../../../../shared/service/app.service";
import { Location } from "@angular/common";
import { Employee } from "../../../../shared/entities/employee";
import { Business } from "../../../../shared/entities/business";
import { AppComponent } from "../../../../app.component";
import { EmployeeOfficalInfo } from "../../../../shared/entities/employee";
import { EmployeeAddressInfo } from "../../../../shared/entities/employee";
import { EmployeeContactInfo } from "../../../../shared/entities/employee";
import { EmployeeEmergencyInfo } from "../../../../shared/entities/employee";
import { DocumentFiles } from "../../../../shared/entities/document";
import { ResizeEvent } from "angular-resizable-element";
@Component({
  selector: "app-add-employee",
  templateUrl: "./add-employee.component.html",
  styleUrls: ["./add-employee.component.css"],
})
export class AddEmployeeComponent implements OnInit {
  employee: Employee = new Employee();
  employeeOfficalInfo: EmployeeOfficalInfo = new EmployeeOfficalInfo();
  employeeAddressInfo: EmployeeAddressInfo = new EmployeeAddressInfo();
  employeeContactInfo: EmployeeContactInfo = new EmployeeContactInfo();
  employeeEmergencyInfo: EmployeeEmergencyInfo = new EmployeeEmergencyInfo();
  document: DocumentFiles = new DocumentFiles();
  lineManagers: any = [];
  languages: any = [];

  adharDocumentPath: any;
  voterIdDocumentPath: any;
  panDocumentPath: any;
  passportDocumentPath: any;
  PhotographDocumentPath: any;
  highestQualification: any;
  YearOfCompletion: any;

  height: any = 1;
  previousHeight: number = 43;
  innerHeight: number;
  width: number = 25;
  maxHeight: number;
  restoreHeight: number;
  selectedTab: any = 0;

  dataSrc: any = [];
  isEdit: boolean = true;
  business: Business = new Business();

  isNodelLabelChange: boolean = false;
  childrenNode: any;
  status: any;
  states: any = [];
  employeObject: any = {};
  constructor(
    private dialog: MatDialog,
    private service: ModuleService,
    private appService: AppService,
    private location: Location,
    public appComponent: AppComponent
  ) {}

  ngOnInit() {
    this.maxHeight = window.innerHeight - 56;
    if (localStorage.getItem("ubt")) {
      //this.ubt = JSON.parse(localStorage.getItem('ubt'))
    }
    if (localStorage.getItem("ubtTab")) {
      this.selectedTab = localStorage.getItem("ubtTab");
    }
    if (localStorage.getItem("ubtHeight")) {
      this.height = localStorage.getItem("ubtHeight");
    } else {
      this.height = 1;
      this.height = window.innerHeight / 2.2;
    }
    //this.getTreeData();
    this.getStates();
    this.getlineManager();
    this.getlanguages();
  }
  minWidth() {
    this.width = 0;
  }
  maxWidth() {
    this.width = 25;
  }
  min() {
    this.height = 1;
  }
  max() {
    if (this.height != window.innerHeight - 90) {
      this.height = window.innerHeight - 90;
    } else {
      this.height = this.previousHeight;
    }
  }
  previous() {
    this.height = this.previousHeight;
  }
  restore() {
    this.height = window.innerHeight / 2.2;
    this.restoreHeight = window.innerHeight / 2.2;
  }
  getlineManager() {
    let URL = "EmployeeMasterApi/GetAllEmployeesNameAndId_SF";
    this.service.getData({}, URL).subscribe((data: any) => {
      console.log(data);
      this.lineManagers = data;
      //console.log(this.dataSrc);
    });
  }
  getTreeData() {
    let object = {
      SchemaName: "dbo",
      EntityCategory: "Master",
      Entity: "BUSINESSTYPE",
      ReturnType: "TREE",
    };
    let URL = "Tree/GetTree_SF";
    this.service.postData(object, URL).subscribe((data: any) => {
      console.log(data);
      this.dataSrc = data.RecursiveObjects;
      console.log(this.dataSrc);
    });
  }
  // addCompany(templateRef: TemplateRef<any>){
  //   this.dialog.open(templateRef,{ disableClose: true });
  // }

  onSubmit(myForm) {
    this.dialog.closeAll();
  }
  refresh() {
    this.getTreeData();
  }
  getStates() {
    let url = "MasterDataApi/GetAllStates_SF";
    this.service.getData({}, url).subscribe((data: any) => {
      console.log(data);
      this.states = data;
    });
  }
  getlanguages() {
    let url = "LanguageApi/GetAllLanguages_SF";
    this.service.getData({}, url).subscribe((data: any) => {
      console.log(data);
      this.languages = data;
    });
  }
  saveData($event) {
    $event["IsActive"] = 1;
    //console.log($event);
    let url = "EmployeeMasterApi/CreateEmployeeMaster";
    this.service.postData($event, url).subscribe((data: any) => {
      console.log(data);
      if (
        data.ReturnStatusMessage == "Employee has been created successfully."
      ) {
        this.getEmployeDetailsById(data.EmployeeId);
        this.appService.showMessage(data.ReturnStatusMessage, "X");
      } else {
        this.appService.showMessage("Somethimg went wrong", "X");
      }
    });
  }
  tabChange(event) {
    // console.log(event)
    this.selectedTab = event.index;
  }
  saveChanges() {}
  discardChanges() {}
  nodeLabel(e) {}
  dataNode(e) {}
  viewDetails() {}
  checkPermission() {}
  saveEmployeeOfficalInfo($event) {
    console.log($event);

    if ($event.EmploymentTypeId == 0) {
      this.employeObject.EmploymentType = "Permanent";
    }
    if ($event.EmploymentTypeId == 1) {
      this.employeObject.EmploymentType = "Contract";
    }
    //this.employeObject.EmploymentType = $event.EmploymentType
    this.employeObject.DateOfJoining = $event.DateOfJoining;
    this.employeObject.DateOfLeaving = $event.DateOfLeaving;
    this.employeObject.ReasonOfLeaving = $event.ReasonOfLeaving;
    this.employeObject.LineManager = $event.LineManager;
    this.employeObject.Action = "UPDATE";
    this.employeObject.IsActive = 1;

    //console.log(this.employeObject);

    let url = "EmployeeMasterApi/UpdateEmployeeMaster";
    this.service.postData(this.employeObject, url).subscribe((data: any) => {
      console.log(data);
      if (
        data.ReturnStatusMessage == "Employee details updated successfully."
      ) {
        this.appService.showMessage(data.ReturnStatusMessage, "X");
      } else {
        this.appService.showMessage("Somethimg went wrong", "X");
      }
    });
  }

  getEmployeDetailsById(EmployeeId) {
    //console.log("hi called");
    this.getStates();
    this.getlineManager();
    this.getlanguages();
    let obj = {
      EmployeeId: EmployeeId,
    };
    let url = "EmployeeMasterApi/GetEmployeeDetailsByEmployeeId_SF";
    this.service.postData(obj, url).subscribe((data: any) => {
      console.log(data);
      if (data.length != 0) {
        this.employee = data[0];
        //this.currentEmployeId = this.employee.EmployeeId;
        this.employeObject = data[0];
      }
    });
  }
  saveEmployeeAddressInfo($event) {
    //console.log($event);
    this.employeObject.HouseNo = $event.HouseNo;
    this.employeObject.Address =
      $event.AddressLine1 +
      " " +
      $event.AddressLine2 +
      " " +
      $event.AddressLine3;
    this.employeObject.LandMark = $event.LandMark;
    this.employeObject.PostOffice = $event.PostOffice;
    this.employeObject.PoliceStation = $event.PoliceStation;
    this.employeObject.City = $event.City;
    this.employeObject.District = $event.District;
    this.employeObject.Pin = $event.Pin;
    this.employeObject.State = $event.State;

    this.employeObject.Action = "UPDATE";
    this.employeObject.IsActive = 1;
    //  console.log(this.employeObject);
    let url = "EmployeeMasterApi/UpdateEmployeeMaster";
    this.service.postData(this.employeObject, url).subscribe((data: any) => {
      console.log(data);
      if (
        data.ReturnStatusMessage == "Employee details updated successfully."
      ) {
        this.appService.showMessage(data.ReturnStatusMessage, "X");
      } else {
        this.appService.showMessage("Somethimg went wrong", "X");
      }
    });
  }
  saveEmployeeContactInfo($event) {
    //console.log($event);
    this.employeObject.Phone1 = $event.Phone1;
    this.employeObject.Phone2 = $event.Phone2;
    this.employeObject.PersonalEmail = $event.PersonalEmail;

    this.employeObject.Action = "UPDATE";
    this.employeObject.IsActive = 1;
    //  console.log(this.employeObject);
    let url = "EmployeeMasterApi/UpdateEmployeeMaster";
    this.service.postData(this.employeObject, url).subscribe((data: any) => {
      console.log(data);
      if (
        data.ReturnStatusMessage == "Employee details updated successfully."
      ) {
        this.appService.showMessage(data.ReturnStatusMessage, "X");
      } else {
        this.appService.showMessage("Somethimg went wrong", "X");
      }
    });
  }
  saveEmployeeEmergencyInfo($event) {
    // console.log($event);
    this.employeObject.EmergencyName = $event.EmergencyContactName;
    this.employeObject.EmergencyPhone1 = $event.EmergencyContactPhone1;
    this.employeObject.EmergencyPhone2 = $event.EmergencyContactPhone2;
    this.employeObject.EmergencyEmail1 = $event.EmergencyContactEmail1;
    this.employeObject.EmergencyEmail2 = $event.EmergencyContactEmail2;
    this.employeObject.EmergencyRelationship =
      $event.EmergencyContactRelationship;
    this.employeObject.EmergencyDependency = $event.EmergencyDependency;
    this.employeObject.EmergencyContactOrganization =
      $event.EmergencyContactOrganization;

    this.employeObject.Action = "UPDATE";
    this.employeObject.IsActive = 1;
    //  console.log(this.employeObject);
    let url = "EmployeeMasterApi/UpdateEmployeeMaster";
    this.service.postData(this.employeObject, url).subscribe((data: any) => {
      console.log(data);
      if (
        data.ReturnStatusMessage == "Employee details updated successfully."
      ) {
        this.appService.showMessage(data.ReturnStatusMessage, "X");
      } else {
        this.appService.showMessage("Somethimg went wrong", "X");
      }
    });
  }
  saveEmployeeDocumentsInfo($event) {
    //console.log($event);
    this.document.EntityCategory = "Master";
    this.document.BusinessId = "";
    this.document.BusinessName = "";
    this.document.CompanyId = "";
    this.document.CompanyName = "";
    this.document.BusinesTypes = "";
    this.document.EntityParent = "";
    this.document.EntityType = "Employees";
    this.document.EntityName =
      this.employeObject.EmployeeId + "_" + this.employeObject.EmployeeName;
    this.document.DocumentType = $event.UploadedFileName;
    this.document.ControlId = "ControlId-1";
    this.document.FileDetails = $event;
    let url = "MasterDataApi/SaveDocument";
    let data = this.document;
    console.log(data);
    this.service.postData(data, url).subscribe((data: any) => {
      console.log(data);
      if (data) {
        this.appService.showMessage("Saved Successfully", "X");
        this.getDocuments(this.document.DocumentType);
        //this.closeDialog();
      } else {
        this.appService.showMessage("Somethimg went wrong", "X");
      }
    });
  }
  getDocuments(type) {
    let data = {
      DocumentInfo: [
        {
          EntityCategory: "Master",
          BusinessId: "",
          BusinessName: "",
          CompanyId: "",
          CompanyName: "",
          BusinesTypes: "",
          EntityParent: "",
          EntityType: "Employees",
          EntityName:
            this.employeObject.EmployeeId +
            "_" +
            this.employeObject.EmployeeName,
          DocumentType: type,
          ControlId: "ControlId-1",
          FilesCount: 1,
        },
      ],
    };
    let url = "FilesApi/GetAllFilesforScreen_SF";
    this.service.postData(data, url).subscribe((data: any) => {
      console.log(data);
      if (type == "ADHAR") {
        if (data && data.AbsolutePath[0].DocumentsAbsolutePath.length != 0) {
          this.adharDocumentPath =
            data.AbsolutePath[0].DocumentsAbsolutePath[0].AbsoluteFilePath;
        } else {
          this.adharDocumentPath = null;
        }
      } else if (type == "VOTERID") {
        if (data && data.AbsolutePath[0].DocumentsAbsolutePath.length != 0) {
          this.voterIdDocumentPath =
            data.AbsolutePath[0].DocumentsAbsolutePath[0].AbsoluteFilePath;
        } else {
          this.voterIdDocumentPath = null;
        }
      } else if (type == "PAN") {
        if (data && data.AbsolutePath[0].DocumentsAbsolutePath.length != 0) {
          this.panDocumentPath =
            data.AbsolutePath[0].DocumentsAbsolutePath[0].AbsoluteFilePath;
        } else {
          this.panDocumentPath = null;
        }
      } else if (type == "PASSPORT") {
        if (data && data.AbsolutePath[0].DocumentsAbsolutePath.length != 0) {
          this.passportDocumentPath =
            data.AbsolutePath[0].DocumentsAbsolutePath[0].AbsoluteFilePath;
        } else {
          this.passportDocumentPath = null;
        }
      } else if (type == "PHOTOGRAPH") {
        if (data && data.AbsolutePath[0].DocumentsAbsolutePath.length != 0) {
          this.PhotographDocumentPath =
            data.AbsolutePath[0].DocumentsAbsolutePath[0].AbsoluteFilePath;
        } else {
          this.PhotographDocumentPath = null;
        }
      } else if (type == "HIGHESTQUALIFICATION") {
        if (data && data.AbsolutePath[0].DocumentsAbsolutePath.length != 0) {
          this.highestQualification =
            data.AbsolutePath[0].DocumentsAbsolutePath[0].AbsoluteFilePath;
        } else {
          this.highestQualification = null;
        }
      } else if (type == "YEAROFCOMPLETION") {
        if (data && data.AbsolutePath[0].DocumentsAbsolutePath.length != 0) {
          this.YearOfCompletion =
            data.AbsolutePath[0].DocumentsAbsolutePath[0].AbsoluteFilePath;
        } else {
          this.YearOfCompletion = null;
        }
      } else {
        this.adharDocumentPath = null;
        this.voterIdDocumentPath = null;
        this.panDocumentPath = null;
        this.passportDocumentPath = null;
        this.PhotographDocumentPath = null;
        this.highestQualification = null;
        this.YearOfCompletion = null;
      }
    });
  }
  onResizeEnd(event: ResizeEvent) {
    if (event) {
      // console.log(event.rectangle.height);
      if (
        event.rectangle.height >= 43 &&
        event.rectangle.height <= window.innerHeight - 56
      ) {
        this.height = event.rectangle.height;
        this.previousHeight = this.height;
      }
    }
  }
}
